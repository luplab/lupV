# LupV: an education-friendly RISC-V based system emulator

## About

**LupV** is an education-friendly RISC-V based system emulator. It aims to be
simple to enough for students to study its code, while being powerful (e.g., it
can boot Linux!).

Main features:
- Support for RV{32,64}IMAFDC (*aka RV{32,64}GC*) ISAs
    * Base RV32I v2.1 or RV64I v2.1
    * Extension M v2.0
    * Extension A v2.1
    * Extension F v2.1
    * Extension D v2.1
    * Extension Zicsr v2.0
    * Extension Zifencei v2.0
    * Extension C v2.0
    * Machine, Supervisor, and User modes
- Education-friendly I/O devices based on the [LupIO
  specifications](https://gitlab.com/luplab/lupio/lupio-specs)
    * Core devices: programmable timer, programmable interrupt controller
    * I/O devices: block device, terminal, real-time clock, random number
      generator
- Complete debugging infrastructure
    - Hardware tracing
    - Integrated GDB server for debugging emulated code
- Native integration of the unopinionated [Dumb
  Bootloader](https://gitlab.com/luplab/riscv-dbl)
- Small and easy to understand implementation
- Fast emulation (boots Linux in a little over half a second!)

## Build and run Linux + BusyBox

First, fetch the code and compile it. By default, it produces a 32-bit emulator.
Use `make help` for more build options.

```console
$ git clone https://gitlab.com/luplab/lupV
$ cd lupV
$ make
```

Then download and decompress the image of the pre-compiled 32-bit Linux kernel
and the image of the [BusyBox](https://busybox.net/)-based root filesystem. (See
[wiki](https://gitlab.com/luplab/lupv/-/wikis/) to learn how to recreate the
whole software stack)

```console
$ wget https://gitlab.com/luplab/lupV/-/wikis/uploads/60c694b0248c6297c8037ae25fe64e6d/Image.gz
$ wget https://gitlab.com/luplab/lupV/-/wikis/uploads/378444f370bfda756d02f5b5aac5c464/rootfs.img.gz
$ gzip -d Image.gz
$ gzip -d rootfs.img.gz
```

Finally, run the emulator.

```console
$ ./lupv32 Image rootfs.img
DBL - the dumb bootloader
[    0.000000] OF: fdt: Ignoring memory range 0x0 - 0x400000
[    0.000000] Linux version 5.8.0-rc3-00156-gb33a220a56c3 (joel@courcelle) (riscv64-linux-gnu-gcc (GCC) 11.1.0, GNU ld (GNU Binutils) 2.36.1) #125 Mon Jul 26 17:11:03 EDT 2021
[    0.000000] earlycon: lupio_tty0 at MMIO 0xc0006000 (options '')
[    0.000000] printk: bootconsole [lupio_tty0] enabled
[    0.000000] Zone ranges:
[    0.000000]   Normal   [mem 0x0000000000400000-0x000000000fffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000000400000-0x000000000fffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000000400000-0x000000000fffffff]
[    0.000000] riscv: ISA extensions acdfimsu
[    0.000000] riscv: ELF capabilities acdfim
...
[    0.548000] Run /sbin/init as init process
[    0.588000] ext2 filesystem being remounted at / supports timestamps until 2038 (0x7fffffff)


BusyBox v1.33.0.git (2021-07-20 18:15:11 EDT) built-in shell (ash)

# ls
bin         etc         lib         proc        sys         usr
dev         init        lost+found  sbin        tmp
# poweroff -f
[    4.488000] reboot: Power down
$
```

You can forcefully end the emulation by typing `Ctrl+a`. Type `Ctrl+h` for help
during the emulation.

## Debugging infrastructure

**LupV** supports hardware tracing for most of its internal components, and
offers a GDB server for debugging the emulated code.

### Hardware tracing

Tracing can be very useful in order to examine the state of the different device
models or of the CPU's internals.

For example, if one wants to get the maximum tracing of the real-time clock, the
regular amount of tracing of the machine, and the mimimum amout of tracing of
the block device:

```console
$ ./lupv32 -d rtc+ -d machine -d blk- Image rootfs.img
machine<2> Loading binary image '/home/joel/lupV/bios/dbl_lupv_32.bin' in RAM at 0x080000000
machine<2> Loading binary image 'Image' in RAM at 0x080400000
DBL - the dumb bootloader
...
rtc<3> lupio_rtc_read: read time = Mon Jul 26 21:57:21 2021
[    0.444000] lupio_rtc c0003000.rtc: setting system clock to 2021-07-26T21:57:21 UTC (1627336641)
...
```

Run emulator with `-d help` to have the list of all tracable items.

### GDB emulated code debugging

**LupV** can run a GDB server, to which a GDB client can get attached.

In one terminal, run the emulator and use argument `-S` to start the GDB server.

```console
$ ./lupv32 -S Image rootfs.img
```

In another terminal, run the GDB client against the ELF executable of the
emulated code (compiled with `-g`).

```console
$ riscv64-linux-gnu-gdb vmlinux
...
(gdb) target remote :1234
Remote debugging using :1234
0xfffe0000 in ?? ()
(gdb) b *0x0040007c
Breakpoint 1 at 0x40007c
(gdb) c
Continuing.

Breakpoint 1, 0x0040007c in ?? ()
(gdb) bt
(gdb) b start_kernel
Breakpoint 2 at 0xc00015de
(gdb) d 1
(gdb) c
Continuing.

Breakpoint 2, 0xc00015de in start_kernel ()
(gdb)
```

## Credit and license

**LupV**
[started](https://gitlab.com/luplab/lupV/-/commit/b1fa843f96fc2dbd53ffdbb72066ed928d597e1d)
as a fork of RISCVEMU (the predecessor of
[TinyEMU](https://bellard.org/tinyemu/)), written by Fabrice Bellard.

**LupV** is developed and supported by Joël Porquet-Lupine at the
[LupLab](https://luplab.cs.ucdavis.edu/) and is released under the [MIT
license](https://opensource.org/licenses/MIT).
