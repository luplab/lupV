/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * RISC-V CPU device
 *  Public API
 */
#ifndef CPU_H
#define CPU_H

#include <inttypes.h>
#include <stdbool.h>

/*
 * Public CPU API, accessible by the entire system
 */

/* Type definitions */
typedef uint64_t phys_addr_t;

#if CPU_XLEN == 32
typedef uint32_t cpu_uint_t;
typedef int32_t cpu_int_t;

#define PRIxcpu "08x"
#define PRIxpa "09" PRIx64

#elif CPU_XLEN == 64
typedef uint64_t cpu_uint_t;
typedef int64_t cpu_int_t;

#define PRIxcpu "016" PRIx64
#define PRIxpa "014" PRIx64

#else
#error Unsupported CPU_XLEN
#endif


/* Memory page definitions for the virtual memory system */
#define PAGE_SHIFT  12                      /* Page bits are the 12 LSBs */
#define PAGE_SIZE   (1UL << PAGE_SHIFT)     /* Pages of size 4 KiB (2^12) */
#define PAGE_MASK   (PAGE_SIZE - 1)         /* Query status of the page bits */

/* IRQ causes */
#define IRQ_U_SOFT      0
#define IRQ_S_SOFT      1
#define IRQ_H_SOFT      2
#define IRQ_M_SOFT      3
#define IRQ_U_TIMER     4
#define IRQ_S_TIMER     5
#define IRQ_H_TIMER     6
#define IRQ_M_TIMER     7
#define IRQ_U_EXT       8
#define IRQ_S_EXT       9
#define IRQ_H_EXT       10
#define IRQ_M_EXT       11

void riscv_cpu_set_irq(int irq, bool state);
bool riscv_cpu_has_timer_irq(void);
bool riscv_cpu_is_stalled(void);

void riscv_cpu_interp(int n_cycles);

void riscv_cpu_init(cpu_uint_t boot_addr);

#endif /* CPU_H */
