/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * RISC-V CPU device
 *  Floating-point instructions template
 *   Extension F v2.1
 *   Extension D v2.1
 */

/*
 * This is a template file that generates the code to emulate the floating-point
 * instructions.
 *
 * If included with F_SIZE == 32, then it provides support for single-precision
 * FP instructions. If included with F_SIZE == 64, then it provides support for
 * double-precision FP instructions.
 */

#if F_SIZE == 32
#   define OPID 0
#elif F_SIZE == 64
#   define OPID 1
#else
#   error unsupported F_SIZE
#endif

#define FSIGN_MASK concat(FSIGN_MASK, F_SIZE)

            case (0x00 << 2) | OPID: /* fadd.[sd] */
                rm = get_insn_rm(s, rm);
                if (rm < 0)
                    goto illegal_insn;
                s->fp_reg[rd] = concat(add_sf, F_SIZE)(s->fp_reg[rs1],
                                         s->fp_reg[rs2],
                                         rm, &s->fflags);
                s->fs = 3;
                break;
            case (0x01 << 2) | OPID: /* fsub.[sd] */
                rm = get_insn_rm(s, rm);
                if (rm < 0)
                    goto illegal_insn;
                s->fp_reg[rd] = concat(sub_sf, F_SIZE)(s->fp_reg[rs1],
                                               s->fp_reg[rs2],
                                               rm, &s->fflags);
                s->fs = 3;
                break;
            case (0x02 << 2) | OPID: /* fmul.[sd] */
                rm = get_insn_rm(s, rm);
                if (rm < 0)
                    goto illegal_insn;
                s->fp_reg[rd] = concat(mul_sf, F_SIZE)(s->fp_reg[rs1],
                                               s->fp_reg[rs2],
                                               rm, &s->fflags);
                s->fs = 3;
                break;
            case (0x03 << 2) | OPID: /* fdiv.[sd] */
                rm = get_insn_rm(s, rm);
                if (rm < 0)
                    goto illegal_insn;
                s->fp_reg[rd] = concat(div_sf, F_SIZE)(s->fp_reg[rs1],
                                               s->fp_reg[rs2],
                                               rm, &s->fflags);
                s->fs = 3;
                break;
            case (0x0b << 2) | OPID: /* fsqrt.[sd] */
                rm = get_insn_rm(s, rm);
                if (rm < 0 || rs2 != 0)
                    goto illegal_insn;
                s->fp_reg[rd] = concat(sqrt_sf, F_SIZE)(s->fp_reg[rs1],
                                                rm, &s->fflags);
                s->fs = 3;
                break;
            case (0x04 << 2) | OPID: /* fsgnj */
                switch(rm) {
                case 0: /* fsgnj.[sd] */
                    s->fp_reg[rd] = (s->fp_reg[rs1] & ~FSIGN_MASK) |
                        (s->fp_reg[rs2] & FSIGN_MASK);
                    break;
                case 1: /* fsgnjn.[sd] */
                    s->fp_reg[rd] = (s->fp_reg[rs1] & ~FSIGN_MASK) |
                        ((s->fp_reg[rs2] & FSIGN_MASK) ^ FSIGN_MASK);
                    break;
                case 2: /* fsgnjx.[sd] */
                    s->fp_reg[rd] = s->fp_reg[rs1] ^
                        (s->fp_reg[rs2] & FSIGN_MASK);
                    break;
                default:
                    goto illegal_insn;
                }
                s->fs = 3;
                break;
            case (0x05 << 2) | OPID: /* fmin-max */
                switch(rm) {
                case 0: /* fmin */
                    s->fp_reg[rd] = concat(min_sf, F_SIZE)(s->fp_reg[rs1],
                                                   s->fp_reg[rs2],
                                                   &s->fflags,
                                                   FMINMAX_IEEE754_201X);
                    break;
                case 1: /* fmax */
                    s->fp_reg[rd] = concat(max_sf, F_SIZE)(s->fp_reg[rs1],
                                                   s->fp_reg[rs2],
                                                   &s->fflags,
                                                   FMINMAX_IEEE754_201X);
                    break;
                default:
                    goto illegal_insn;
                }
                s->fs = 3;
                break;
            case (0x18 << 2) | OPID: /* fcvt.int.fmt */
                rm = get_insn_rm(s, rm);
                if (rm < 0)
                    goto illegal_insn;
                switch(rs2) {
                case 0: /* fcvt.w.[sd] */
                    val = (int32_t)concat3(cvt_sf, F_SIZE, _i32)(s->fp_reg[rs1], rm,
                                                          &s->fflags);
                    break;
                case 1: /* fcvt.wu.[sd] */
                    val = (int32_t)concat3(cvt_sf, F_SIZE, _u32)(s->fp_reg[rs1], rm,
                                                          &s->fflags);
                    break;
#if CPU_XLEN == 64
                case 2: /* fcvt.l.[sd] */
                    val = (int64_t)concat3(cvt_sf, F_SIZE, _i64)(s->fp_reg[rs1], rm,
                                                          &s->fflags);
                    break;
                case 3: /* fcvt.lu.[sd] */
                    val = (int64_t)concat3(cvt_sf, F_SIZE, _u64)(s->fp_reg[rs1], rm,
                                                          &s->fflags);
                    break;
#endif
                default:
                    goto illegal_insn;
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;
            case (0x14 << 2) | OPID: /* fcmp */
                switch(rm) {
                case 0: /* fle */
                    val = concat(le_sf, F_SIZE)(s->fp_reg[rs1], s->fp_reg[rs2],
                                     &s->fflags);
                    break;
                case 1: /* flt */
                    val = concat(lt_sf, F_SIZE)(s->fp_reg[rs1], s->fp_reg[rs2],
                                     &s->fflags);
                    break;
                case 2: /* feq */
                    val = concat(eq_quiet_sf, F_SIZE)(s->fp_reg[rs1], s->fp_reg[rs2],
                                           &s->fflags);
                    break;
                default:
                    goto illegal_insn;
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;
            case (0x1a << 2) | OPID: /* fcvt.fmt.int */
                rm = get_insn_rm(s, rm);
                if (rm < 0)
                    goto illegal_insn;
                switch(rs2) {
                case 0: /* fcvt.[sd].w */
                    s->fp_reg[rd] = concat(cvt_i32_sf, F_SIZE)(s->reg[rs1], rm,
                                                       &s->fflags);
                    break;
                case 1: /* fcvt.[sd].wu */
                    s->fp_reg[rd] = concat(cvt_u32_sf, F_SIZE)(s->reg[rs1], rm,
                                                       &s->fflags);
                    break;
#if CPU_XLEN == 64
                case 2: /* fcvt.[sd].l */
                    s->fp_reg[rd] = concat(cvt_i64_sf, F_SIZE)(s->reg[rs1], rm,
                                                       &s->fflags);
                    break;
                case 3: /* fcvt.[sd].lu */
                    s->fp_reg[rd] = concat(cvt_u64_sf, F_SIZE)(s->reg[rs1], rm,
                                                       &s->fflags);
                    break;
#endif
                default:
                    goto illegal_insn;
                }
                s->fs = 3;
                break;

            case (0x08 << 2) | OPID: /* fcvt.fmt.fmt */
                rm = get_insn_rm(s, rm);
                if (rm < 0)
                    goto illegal_insn;
                switch(rs2) {
#if F_SIZE == 32
                case 1: /* cvt.s.d */
                    s->fp_reg[rd] = cvt_sf64_sf32(s->fp_reg[rs1], rm, &s->fflags);
                    break;
#endif
#if F_SIZE == 64
                case 0: /* cvt.d.s */
                    s->fp_reg[rd] = cvt_sf32_sf64(s->fp_reg[rs1], &s->fflags);
                    break;
#endif

                default:
                    goto illegal_insn;
                }
                s->fs = 3;
                break;

            case (0x1c << 2) | OPID: /* {fclass,fmv} */
                if (rs2 != 0)
                    goto illegal_insn;
                switch(rm) {
                case 0: /* fmv.x.w */
#if F_SIZE == 32
                    val = (int32_t)s->fp_reg[rs1];
#elif F_SIZE == 64
                    val = (int64_t)s->fp_reg[rs1];
#endif
                    break;
                case 1: /* fclass */
                    val = concat(fclass_sf, F_SIZE)(s->fp_reg[rs1]);
                    break;
                default:
                    goto illegal_insn;
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;

            case (0x1e << 2) | OPID: /* fmv.w.x */
                if (rs2 != 0 || rm != 0)
                    goto illegal_insn;
#if F_SIZE == 32
                s->fp_reg[rd] = (int32_t)s->reg[rs1];
#elif F_SIZE == 64
                s->fp_reg[rd] = (int64_t)s->reg[rs1];
#endif
                s->fs = 3;
                break;

#undef F_SIZE
#undef OPID
#undef FSIGN_MASK

