/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * RISC-V CPU device
 *  Instruction interpreter for typical RV32/64 GC ISA
 *   Base RV32I/RV64I v2.1
 *   Extension M v2.0
 *   Extension A v2.1
 *   Extension F v2.1
 *   Extension D v2.1
 *   Extension Zicsr v2.0
 *   Extension Zifencei v2.0
 *   Extension C v2.0
 */

#include <assert.h>
#include <inttypes.h>

#include <cpu/cpu_private.h>
#define DBG_ITEM DBG_CPU_INST
#include <lib/debug.h>
#include <lib/gdb.h>
#include <lib/softfp/softfp.h>
#include <lib/utils.h>

/* Supported extensions */
#define RISCV_EXT_M         /* Integer Multiplication and Division */
#define RISCV_EXT_A         /* Atomic Instructions */
#define RISCV_EXT_FD        /* Single- and Double-Precision Floating-Point */
#define RISCV_EXT_Zicsr     /* Control and Status Register (CSR) Instructions */
#define RISCV_EXT_Zifencei  /* Instruction-Fetch Fence */
#define RISCV_EXT_C         /* Compressed instructions */

static void dump_regs(RISCVCPUState *s)
{
    const static char *reg_name[NREGS] = {
        "zro", "ra",   "sp",   "gp",   "tp",   "t0",   "t1",   "t2",
        "s0",   "s1",   "a0",   "a1",   "a2",   "a3",   "a4",   "a5",
        "a6",   "a7",   "s2",   "s3",   "s4",   "s5",   "s6",   "s7",
        "s8",   "s9",   "s10",  "s11",  "t3",   "t4",   "t5",   "t6"
    };
    const static char priv_str[4] = "USHM";

    int i, len;
    char buffer[1024] = {0};

    len = sprintf(buffer, "priv=%c mstatus=0x%" PRIxcpu " cycles=%" PRId64 "\n",
                  priv_str[s->priv], s->mstatus, s->insn_counter);
    for(i = 0; i < NREGS; i++) {
        len += sprintf(buffer + len,
                       " %-3s=0x%" PRIxcpu, reg_name[i], s->reg[i]);
        if ((i & 7) == 7 && i != 31)
            len += sprintf(buffer + len,"\n");
    }
    dbg_more("%s", buffer);

    dbg_more("mideleg=0x%" PRIxcpu " mie=0x%" PRIxcpu " mip=0x%" PRIxcpu,
             s->mideleg, s->mie, s->mip);
}

static int get_insn_rm(RISCVCPUState *s, unsigned int rm)
{
    if (rm == 7)
        return s->frm;
    else if (rm >= 5)
        return -1;
    else
        return rm;
}

static inline uint32_t get_insn_field(uint32_t val, int src_pos,
                                      int dst_pos, int dst_pos_max)
{
    int mask;

    assert(dst_pos_max >= dst_pos);

    mask = ((1 << (dst_pos_max - dst_pos + 1)) - 1) << dst_pos;
    if (dst_pos >= src_pos)
        return (val << (dst_pos - src_pos)) & mask;
    else
        return (val >> (src_pos - dst_pos)) & mask;
}

static inline int32_t sign_ext(int32_t val, int n)
{
    return (val << (32 - n)) >> (32 - n);
}

#define DIV(type, _a, _b)                                           \
({                                                                  \
    type a = _a;                                                    \
    type b = _b;                                                    \
    type r;                                                         \
                                                                    \
    if (b == 0)                                                     \
        r = -1;                                                     \
    else if (a == ((type)1 << ((sizeof(type) * 8) - 1)) && b == -1) \
        r = a;                                                      \
    else                                                            \
        r = a / b;                                                  \
    r;                                                              \
})

#define DIVU(type, _a, _b)                 \
({                                         \
    type a = _a;                           \
    type b = _b;                           \
    type r;                                \
                                           \
    if (b == 0)                            \
        r = -1;                            \
    else                                   \
        r = a / b;                         \
    r;                                     \
})

#define REM(type, _a, _b)                                           \
({                                                                  \
    type a = _a;                                                    \
    type b = _b;                                                    \
    type r;                                                         \
                                                                    \
    if (b == 0)                                                     \
        r = a;                                                      \
    else if (a == ((type)1 << ((sizeof(type) * 8) - 1)) && b == -1) \
        r = 0;                                                      \
    else                                                            \
        r = a % b;                                                  \
    r;                                                              \
})

#define REMU(type, _a, _b)                                    \
({                                                            \
    type a = _a;                                              \
    type b = _b;                                              \
    type r;                                                   \
                                                              \
    if (b == 0)                                               \
        r = a;                                                \
    else                                                      \
        r = a % b;                                            \
    r;                                                        \
})

#if CPU_XLEN == 32
static inline uint32_t mulh(int32_t a, int32_t b)
{
    return ((int64_t)a * (int64_t)b) >> 32;
}

static inline uint32_t mulhsu(int32_t a, uint32_t b)
{
    return ((int64_t)a * (int64_t)b) >> 32;
}

static inline uint32_t mulhu(uint32_t a, uint32_t b)
{
    return ((int64_t)a * (int64_t)b) >> 32;
}
#else
static cpu_uint_t mulhu(cpu_uint_t a, cpu_uint_t b)
{
    uint32_t a0, a1, b0, b1, r2, r3;
    cpu_uint_t r00, r01, r10, r11, c;
    a0 = a;
    a1 = a >> 32;
    b0 = b;
    b1 = b >> 32;

    r00 = (cpu_uint_t)a0 * (cpu_uint_t)b0;
    r01 = (cpu_uint_t)a0 * (cpu_uint_t)b1;
    r10 = (cpu_uint_t)a1 * (cpu_uint_t)b0;
    r11 = (cpu_uint_t)a1 * (cpu_uint_t)b1;

    //    r0 = r00;
    c = (r00 >> 32) + (uint32_t)r01 + (uint32_t)r10;
    //    r1 = c;
    c = (c >> 32) + (r01 >> 32) + (r10 >> 32) + (uint32_t)r11;
    r2 = c;
    r3 = (c >> 32) + (r11 >> 32);

    //    *plow = ((cpu_uint_t)r1 << 32) | r0;
    return ((cpu_uint_t)r3 << 32) | r2;
}

static inline cpu_uint_t mulh(cpu_int_t a, cpu_int_t b)
{
    cpu_uint_t r1;
    r1 = mulhu(a, b);
    if (a < 0)
        r1 -= a;
    if (b < 0)
        r1 -= b;
    return r1;
}

static inline cpu_uint_t mulhsu(cpu_int_t a, cpu_uint_t b)
{
    cpu_uint_t r1;
    r1 = mulhu(a, b);
    if (a < 0)
        r1 -= a;
    return r1;
}
#endif

void riscv_cpu_interp_rv(RISCVCPUState *s, int n_cycles)
{
    uint32_t opcode, insn, rd, rs1, rs2, funct3;
    int32_t imm, cond, err;
    cpu_uint_t addr, pc_next, val, val2;
    uint32_t rs3;
    int32_t rm;

    while (n_cycles != 0) {
        /*
         * Before executing the next instruction, check for pending interrupts
         */
        if (unlikely((s->mip & s->mie) != 0))
            raise_interrupt(s);

        dbg_info("pc=0x%" PRIxcpu, s->pc);
        if (unlikely(dbg_item_get_level(DBG_ITEM) > DBG_LVL_INFO))
            dump_regs(s);

        gdb_check_breakpoint(s->pc);

        /*
         * Instruction fetch
         */
        if (mem_read_insn(s, &insn, s->pc))
            goto mmu_exception;
        pc_next = (cpu_int_t)(s->pc + 4);

        /*
         * Instruction decode and execute
         */
        opcode = insn & 0x7f;
        rd = (insn >> 7) & 0x1f;
        rs1 = (insn >> 15) & 0x1f;
        rs2 = (insn >> 20) & 0x1f;

        /* Distinguish between major opcodes (inst[6:0]) */
        switch(opcode) {

#ifdef RISCV_EXT_C
/*
 * Major opcode are organized in quadrants according to inst[1:0]
 * - Quadrants 0 (b00), 1 (b01), 2 (b10) are all reserved for compressed
 *   instructions
 * - Quadrant 3 (b11) is associated to regular word instructions
 */
#define C_QUADRANT(n) \
    case n + (0 << 2):  case n + (1 << 2):  case n + (2 << 2):  case n + (3 << 2):  \
    case n + (4 << 2):  case n + (5 << 2):  case n + (6 << 2):  case n + (7 << 2):  \
    case n + (8 << 2):  case n + (9 << 2):  case n + (10 << 2): case n + (11 << 2): \
    case n + (12 << 2): case n + (13 << 2): case n + (14 << 2): case n + (15 << 2): \
    case n + (16 << 2): case n + (17 << 2): case n + (18 << 2): case n + (19 << 2): \
    case n + (20 << 2): case n + (21 << 2): case n + (22 << 2): case n + (23 << 2): \
    case n + (24 << 2): case n + (25 << 2): case n + (26 << 2): case n + (27 << 2): \
    case n + (28 << 2): case n + (29 << 2): case n + (30 << 2): case n + (31 << 2):

            C_QUADRANT(0)
                pc_next = (cpu_int_t)(s->pc + 2);
                funct3 = (insn >> 13) & 7;
                rd = ((insn >> 2) & 7) | 8;
                switch(funct3) {
                    case 0: /* c.addi4spn */
                        imm = get_insn_field(insn, 11, 4, 5) |
                            get_insn_field(insn, 7, 6, 9) |
                            get_insn_field(insn, 6, 2, 2) |
                            get_insn_field(insn, 5, 3, 3);
                        if (imm == 0)
                            goto illegal_insn;
                        s->reg[rd] = (cpu_int_t)(s->reg[2] + imm);
                        break;
                    case 1: /* c.fld */
                        {
                            uint64_t rval;
                            if (s->fs == 0)
                                goto illegal_insn;
                            imm = get_insn_field(insn, 10, 3, 5) |
                                get_insn_field(insn, 5, 6, 7);
                            rs1 = ((insn >> 7) & 7) | 8;
                            addr = (cpu_int_t)(s->reg[rs1] + imm);
                            if(mem_read_u64(s, &rval, addr))
                                goto mmu_exception;
                            s->fp_reg[rd] = rval;
                            s->fs = 3;
                        }
                        break;
                    case 2: /* c.lw */
                        {
                            uint32_t rval;
                            imm = get_insn_field(insn, 10, 3, 5) |
                                get_insn_field(insn, 6, 2, 2) |
                                get_insn_field(insn, 5, 6, 6);
                            rs1 = ((insn >> 7) & 7) | 8;
                            addr = (cpu_int_t)(s->reg[rs1] + imm);
                            if (mem_read_u32(s, &rval, addr))
                                goto mmu_exception;
                            s->reg[rd] = (int32_t)rval;
                        }
                        break;
#if CPU_XLEN == 64
                    case 3: /* c.ld */
                        {
                            uint64_t rval;
                            imm = get_insn_field(insn, 10, 3, 5) |
                                get_insn_field(insn, 5, 6, 7);
                            rs1 = ((insn >> 7) & 7) | 8;
                            addr = (cpu_int_t)(s->reg[rs1] + imm);
                            if (mem_read_u64(s, &rval, addr))
                                goto mmu_exception;
                            s->reg[rd] = (int64_t)rval;
                        }
                        break;
#else
                    case 3: /* c.flw */
                        {
                            uint32_t rval;
                            if (s->fs == 0)
                                goto illegal_insn;
                            imm = get_insn_field(insn, 10, 3, 5) |
                                get_insn_field(insn, 6, 2, 2) |
                                get_insn_field(insn, 5, 6, 6);
                            rs1 = ((insn >> 7) & 7) | 8;
                            addr = (cpu_int_t)(s->reg[rs1] + imm);
                            if (mem_read_u32(s, &rval, addr))
                                goto mmu_exception;
                            s->fp_reg[rd] = rval;
                            s->fs = 3;
                        }
                        break;
#endif
                    case 5: /* c.fsd */
                        if (s->fs == 0)
                            goto illegal_insn;
                        imm = get_insn_field(insn, 10, 3, 5) |
                            get_insn_field(insn, 5, 6, 7);
                        rs1 = ((insn >> 7) & 7) | 8;
                        addr = (cpu_int_t)(s->reg[rs1] + imm);
                        if (mem_write_u64(s, addr, s->fp_reg[rd]))
                            goto mmu_exception;
                        break;
                    case 6: /* c.sw */
                        imm = get_insn_field(insn, 10, 3, 5) |
                            get_insn_field(insn, 6, 2, 2) |
                            get_insn_field(insn, 5, 6, 6);
                        rs1 = ((insn >> 7) & 7) | 8;
                        addr = (cpu_int_t)(s->reg[rs1] + imm);
                        val = s->reg[rd];
                        if (mem_write_u32(s, addr, val))
                            goto mmu_exception;
                        break;
#if CPU_XLEN == 64
                    case 7: /* c.sd */
                        imm = get_insn_field(insn, 10, 3, 5) |
                            get_insn_field(insn, 5, 6, 7);
                        rs1 = ((insn >> 7) & 7) | 8;
                        addr = (cpu_int_t)(s->reg[rs1] + imm);
                        val = s->reg[rd];
                        if (mem_write_u64(s, addr, val))
                            goto mmu_exception;
                        break;
#else
                    case 7: /* c.fsw */
                        if (s->fs == 0)
                            goto illegal_insn;
                        imm = get_insn_field(insn, 10, 3, 5) |
                            get_insn_field(insn, 6, 2, 2) |
                            get_insn_field(insn, 5, 6, 6);
                        rs1 = ((insn >> 7) & 7) | 8;
                        addr = (cpu_int_t)(s->reg[rs1] + imm);
                        if (mem_write_u32(s, addr, s->fp_reg[rd]))
                            goto mmu_exception;
                        break;
#endif
                    default:
                        goto illegal_insn;
                }
                break;

            C_QUADRANT(1)
                pc_next = (cpu_int_t)(s->pc + 2);
                funct3 = (insn >> 13) & 7;
                switch(funct3) {
                    case 0: /* c.addi/c.nop */
                        if (rd != 0) {
                            imm = sign_ext(get_insn_field(insn, 12, 5, 5) |
                                           get_insn_field(insn, 2, 0, 4), 6);
                            s->reg[rd] = (cpu_int_t)(s->reg[rd] + imm);
                        }
                        break;
#if CPU_XLEN == 32
                    case 1: /* c.jal */
                        imm = sign_ext(get_insn_field(insn, 12, 11, 11) |
                                       get_insn_field(insn, 11, 4, 4) |
                                       get_insn_field(insn, 9, 8, 9) |
                                       get_insn_field(insn, 8, 10, 10) |
                                       get_insn_field(insn, 7, 6, 6) |
                                       get_insn_field(insn, 6, 7, 7) |
                                       get_insn_field(insn, 3, 1, 3) |
                                       get_insn_field(insn, 2, 5, 5), 12);
                        s->reg[1] = pc_next;
                        pc_next = (cpu_int_t)(s->pc + imm);
                        break;
#else
                    case 1: /* c.addiw */
                        if (rd != 0) {
                            imm = sign_ext(get_insn_field(insn, 12, 5, 5) |
                                           get_insn_field(insn, 2, 0, 4), 6);
                            s->reg[rd] = (int32_t)(s->reg[rd] + imm);
                        }
                        break;
#endif
                    case 2: /* c.li */
                        if (rd != 0) {
                            imm = sign_ext(get_insn_field(insn, 12, 5, 5) |
                                           get_insn_field(insn, 2, 0, 4), 6);
                            s->reg[rd] = imm;
                        }
                        break;
                    case 3:
                        if (rd == 2) {
                            /* c.addi16sp */
                            imm = sign_ext(get_insn_field(insn, 12, 9, 9) |
                                           get_insn_field(insn, 6, 4, 4) |
                                           get_insn_field(insn, 5, 6, 6) |
                                           get_insn_field(insn, 3, 7, 8) |
                                           get_insn_field(insn, 2, 5, 5), 10);
                            if (imm == 0)
                                goto illegal_insn;
                            s->reg[2] = (cpu_int_t)(s->reg[2] + imm);
                        } else if (rd != 0) {
                            /* c.lui */
                            imm = sign_ext(get_insn_field(insn, 12, 17, 17) |
                                           get_insn_field(insn, 2, 12, 16), 18);
                            s->reg[rd] = imm;
                        }
                        break;
                    case 4:
                        funct3 = (insn >> 10) & 3;
                        rd = ((insn >> 7) & 7) | 8;
                        switch(funct3) {
                            case 0: /* c.srli */
                            case 1: /* c.srai */
                                imm = get_insn_field(insn, 12, 5, 5) |
                                    get_insn_field(insn, 2, 0, 4);
#if CPU_XLEN == 32
                                if (imm & 0x20)
                                    goto illegal_insn;
#endif
                                if (funct3 == 0)
                                    s->reg[rd] = (cpu_int_t)((cpu_uint_t)s->reg[rd] >> imm);
                                else
                                    s->reg[rd] = (cpu_int_t)s->reg[rd] >> imm;

                                break;
                            case 2: /* c.andi */
                                imm = sign_ext(get_insn_field(insn, 12, 5, 5) |
                                               get_insn_field(insn, 2, 0, 4), 6);
                                s->reg[rd] &= imm;
                                break;
                            case 3:
                                rs2 = ((insn >> 2) & 7) | 8;
                                funct3 = ((insn >> 5) & 3) | ((insn >> (12 - 2)) & 4);
                                switch(funct3) {
                                    case 0: /* c.sub */
                                        s->reg[rd] = (cpu_int_t)(s->reg[rd] - s->reg[rs2]);
                                        break;
                                    case 1: /* c.xor */
                                        s->reg[rd] = s->reg[rd] ^ s->reg[rs2];
                                        break;
                                    case 2: /* c.or */
                                        s->reg[rd] = s->reg[rd] | s->reg[rs2];
                                        break;
                                    case 3: /* c.and */
                                        s->reg[rd] = s->reg[rd] & s->reg[rs2];
                                        break;
#if CPU_XLEN == 64
                                    case 4: /* c.subw */
                                        s->reg[rd] = (int32_t)(s->reg[rd] - s->reg[rs2]);
                                        break;
                                    case 5: /* c.addw */
                                        s->reg[rd] = (int32_t)(s->reg[rd] + s->reg[rs2]);
                                        break;
#endif
                                    default:
                                        goto illegal_insn;
                                }
                                break;
                        }
                        break;
                    case 5: /* c.j */
                        imm = sign_ext(get_insn_field(insn, 12, 11, 11) |
                                       get_insn_field(insn, 11, 4, 4) |
                                       get_insn_field(insn, 9, 8, 9) |
                                       get_insn_field(insn, 8, 10, 10) |
                                       get_insn_field(insn, 7, 6, 6) |
                                       get_insn_field(insn, 6, 7, 7) |
                                       get_insn_field(insn, 3, 1, 3) |
                                       get_insn_field(insn, 2, 5, 5), 12);
                        pc_next = (cpu_int_t)(s->pc + imm);
                        break;
                    case 6: /* c.beqz */
                        rs1 = ((insn >> 7) & 7) | 8;
                        imm = sign_ext(get_insn_field(insn, 12, 8, 8) |
                                       get_insn_field(insn, 10, 3, 4) |
                                       get_insn_field(insn, 5, 6, 7) |
                                       get_insn_field(insn, 3, 1, 2) |
                                       get_insn_field(insn, 2, 5, 5), 9);
                        if (s->reg[rs1] == 0)
                            pc_next = (cpu_int_t)(s->pc + imm);
                        break;
                    case 7: /* c.bnez */
                        rs1 = ((insn >> 7) & 7) | 8;
                        imm = sign_ext(get_insn_field(insn, 12, 8, 8) |
                                       get_insn_field(insn, 10, 3, 4) |
                                       get_insn_field(insn, 5, 6, 7) |
                                       get_insn_field(insn, 3, 1, 2) |
                                       get_insn_field(insn, 2, 5, 5), 9);
                        if (s->reg[rs1] != 0)
                            pc_next = (cpu_int_t)(s->pc + imm);
                        break;
                    default:
                        goto illegal_insn;
                }
                break;

            C_QUADRANT(2)
                pc_next = (cpu_int_t)(s->pc + 2);
                funct3 = (insn >> 13) & 7;
                rs2 = (insn >> 2) & 0x1f;
                switch(funct3) {
                    case 0: /* c.slli */
                        imm = get_insn_field(insn, 12, 5, 5) | rs2;
#if CPU_XLEN == 32
                        if (imm & 0x20)
                            goto illegal_insn;
#endif
                        if (rd != 0)
                            s->reg[rd] = (cpu_int_t)(s->reg[rd] << imm);
                        break;
                    case 1: /* c.fldsp */
                        {
                            uint64_t rval;
                            if (s->fs == 0)
                                goto illegal_insn;
                            imm = get_insn_field(insn, 12, 5, 5) |
                                (rs2 & (3 << 3)) |
                                get_insn_field(insn, 2, 6, 8);
                            addr = (cpu_int_t)(s->reg[2] + imm);
                            if(mem_read_u64(s, &rval, addr))
                                goto mmu_exception;
                            s->fp_reg[rd] = rval;
                            s->fs = 3;
                        }
                        break;
                    case 2: /* c.lwsp */
                        {
                            uint32_t rval;
                            imm = get_insn_field(insn, 12, 5, 5) |
                                (rs2 & (7 << 2)) |
                                get_insn_field(insn, 2, 6, 7);
                            addr = (cpu_int_t)(s->reg[2] + imm);
                            if (mem_read_u32(s, &rval, addr))
                                goto mmu_exception;
                            if (rd != 0)
                                s->reg[rd] = (int32_t)rval;
                        }
                        break;
#if CPU_XLEN == 64
                    case 3: /* c.ldsp */
                        {
                            uint64_t rval;
                            imm = get_insn_field(insn, 12, 5, 5) |
                                (rs2 & (3 << 3)) |
                                get_insn_field(insn, 2, 6, 8);
                            addr = (cpu_int_t)(s->reg[2] + imm);
                            if(mem_read_u64(s, &rval, addr))
                                goto mmu_exception;
                            if (rd != 0)
                                s->reg[rd] = (int64_t)rval;
                        }
                        break;
#else
                    case 3: /* c.flwsp */
                        {
                            uint32_t rval;
                            if (s->fs == 0)
                                goto illegal_insn;
                            imm = get_insn_field(insn, 12, 5, 5) |
                                (rs2 & (7 << 2)) |
                                get_insn_field(insn, 2, 6, 7);
                            addr = (cpu_int_t)(s->reg[2] + imm);
                            if (mem_read_u32(s, &rval, addr))
                                goto mmu_exception;
                            s->fp_reg[rd] = rval;
                            s->fs = 3;
                        }
                        break;
#endif
                    case 4:
                        if (((insn >> 12) & 1) == 0) {
                            if (rs2 == 0) {
                                /* c.jr */
                                if (rd == 0)
                                    goto illegal_insn;
                                pc_next = s->reg[rd] & ~1;
                            } else {
                                /* c.mv */
                                if (rd != 0)
                                    s->reg[rd] = s->reg[rs2];
                            }
                        } else {
                            if (rs2 == 0) {
                                if (rd == 0) {
                                    /* c.ebreak */
                                    raise_exception(s, CAUSE_BREAKPOINT);
                                    goto done_interp;
                                } else {
                                    /* c.jalr */
                                    addr = s->reg[rd] & ~1;
                                    s->reg[1] = pc_next;
                                    pc_next = addr;
                                }
                            } else {
                                if (rd != 0) {
                                    s->reg[rd] = (cpu_int_t)(s->reg[rd] + s->reg[rs2]);
                                }
                            }
                        }
                        break;
                    case 5: /* c.fsdsp */
                        if (s->fs == 0)
                            goto illegal_insn;
                        imm = get_insn_field(insn, 10, 3, 5) |
                            get_insn_field(insn, 7, 6, 8);
                        addr = (cpu_int_t)(s->reg[2] + imm);
                        if (mem_write_u64(s, addr, s->fp_reg[rs2]))
                            goto mmu_exception;
                        break;
                    case 6: /* c.swsp */
                        imm = get_insn_field(insn, 9, 2, 5) |
                            get_insn_field(insn, 7, 6, 7);
                        addr = (cpu_int_t)(s->reg[2] + imm);
                        if (mem_write_u32(s, addr, s->reg[rs2]))
                            goto mmu_exception;
                        break;
#if CPU_XLEN == 64
                    case 7: /* c.sdsp */
                        imm = get_insn_field(insn, 10, 3, 5) |
                            get_insn_field(insn, 7, 6, 8);
                        addr = (cpu_int_t)(s->reg[2] + imm);
                        if (mem_write_u64(s, addr, s->reg[rs2]))
                            goto mmu_exception;
                        break;
#else
                    case 7: /* c.swsp */
                        if (s->fs == 0)
                            goto illegal_insn;
                        imm = get_insn_field(insn, 9, 2, 5) |
                            get_insn_field(insn, 7, 6, 7);
                        addr = (cpu_int_t)(s->reg[2] + imm);
                        if (mem_write_u32(s, addr, s->fp_reg[rs2]))
                            goto mmu_exception;
                        break;
#endif
                    default:
                        goto illegal_insn;
                }
                break;
#endif

            case 0x37: /* lui */
                if (rd != 0)
                    s->reg[rd] = (int32_t)(insn & 0xfffff000);
                break;

            case 0x17: /* auipc */
                if (rd != 0)
                    s->reg[rd] = (cpu_int_t)(s->pc + (int32_t)(insn & 0xfffff000));
                break;

            case 0x6f: /* jal */
                imm = ((insn >> (31 - 20)) & (1 << 20)) |
                    ((insn >> (21 - 1)) & 0x7fe) |
                    ((insn >> (20 - 11)) & (1 << 11)) |
                    (insn & 0xff000);
                imm = (imm << 11) >> 11;
                if (rd != 0)
                    s->reg[rd] = pc_next;
                pc_next = (cpu_int_t)(s->pc + imm);
                break;

            case 0x67: /* jalr */
                imm = (int32_t)insn >> 20;
                addr = (cpu_int_t)(s->reg[rs1] + imm) & ~1;
                if (rd != 0)
                    s->reg[rd] = pc_next;
                pc_next = addr;
                break;

            case 0x63: /* branch */
                funct3 = (insn >> 12) & 7;
                switch(funct3 >> 1) {
                    case 0: /* beq/bne */
                        cond = (s->reg[rs1] == s->reg[rs2]);
                        break;
                    case 2: /* blt/bge */
                        cond = ((cpu_int_t)s->reg[rs1] < (cpu_int_t)s->reg[rs2]);
                        break;
                    case 3: /* bltu/bgeu */
                        cond = (s->reg[rs1] < s->reg[rs2]);
                        break;
                    default:
                        goto illegal_insn;
                }
                cond ^= (funct3 & 1);
                if (cond) {
                    imm = ((insn >> (31 - 12)) & (1 << 12)) |
                        ((insn >> (25 - 5)) & 0x7e0) |
                        ((insn >> (8 - 1)) & 0x1e) |
                        ((insn << (11 - 7)) & (1 << 11));
                    imm = (imm << 19) >> 19;
                    pc_next = (cpu_int_t)(s->pc + imm);
                }
                break;

            case 0x03: /* load */
                funct3 = (insn >> 12) & 7;
                imm = (int32_t)insn >> 20;
                addr = s->reg[rs1] + imm;
                switch(funct3) {
                    case 0: /* lb */
                        {
                            uint8_t rval;
                            if (mem_read_u8(s, &rval, addr))
                                goto mmu_exception;
                            val = (int8_t)rval;
                        }
                        break;
                    case 1: /* lh */
                        {
                            uint16_t rval;
                            if (mem_read_u16(s, &rval, addr))
                                goto mmu_exception;
                            val = (int16_t)rval;
                        }
                        break;
                    case 2: /* lw */
                        {
                            uint32_t rval;
                            if (mem_read_u32(s, &rval, addr))
                                goto mmu_exception;
                            val = (int32_t)rval;
                        }
                        break;
                    case 4: /* lbu */
                        {
                            uint8_t rval;
                            if (mem_read_u8(s, &rval, addr))
                                goto mmu_exception;
                            val = rval;
                        }
                        break;
                    case 5: /* lhu */
                        {
                            uint16_t rval;
                            if (mem_read_u16(s, &rval, addr))
                                goto mmu_exception;
                            val = rval;
                        }
                        break;
#if CPU_XLEN == 64
                    case 3: /* ld */
                        {
                            uint64_t rval;
                            if (mem_read_u64(s, &rval, addr))
                                goto mmu_exception;
                            val = (int64_t)rval;
                        }
                        break;
                    case 6: /* lwu */
                        {
                            uint32_t rval;
                            if (mem_read_u32(s, &rval, addr))
                                goto mmu_exception;
                            val = rval;
                        }
                        break;
#endif
                    default:
                        goto illegal_insn;
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;

            case 0x23: /* store */
                funct3 = (insn >> 12) & 7;
                imm = rd | ((insn >> (25 - 5)) & 0xfe0);
                imm = (imm << 20) >> 20;
                addr = s->reg[rs1] + imm;
                val = s->reg[rs2];
                switch(funct3) {
                    case 0: /* sb */
                        if (mem_write_u8(s, addr, val))
                            goto mmu_exception;
                        break;
                    case 1: /* sh */
                        if (mem_write_u16(s, addr, val))
                            goto mmu_exception;
                        break;
                    case 2: /* sw */
                        if (mem_write_u32(s, addr, val))
                            goto mmu_exception;
                        break;
#if CPU_XLEN == 64
                    case 3: /* sd */
                        if (mem_write_u64(s, addr, val))
                            goto mmu_exception;
                        break;
#endif
                    default:
                        goto illegal_insn;
                }
                break;

            case 0x13: /* op-imm */
                funct3 = (insn >> 12) & 7;
                imm = (int32_t)insn >> 20;
                switch(funct3) {
                    case 0: /* addi */
                        val = (cpu_int_t)(s->reg[rs1] + imm);
                        break;
                    case 1: /* slli */
                        if ((imm & ~(CPU_XLEN - 1)) != 0)
                            goto illegal_insn;
                        val = (cpu_int_t)(s->reg[rs1] << (imm & (CPU_XLEN - 1)));
                        break;
                    case 2: /* slti */
                        val = (cpu_int_t)s->reg[rs1] < (cpu_int_t)imm;
                        break;
                    case 3: /* sltiu */
                        val = s->reg[rs1] < (cpu_uint_t)imm;
                        break;
                    case 4: /* xori */
                        val = s->reg[rs1] ^ imm;
                        break;
                    case 5: /* srli/srai */
                        if ((imm & ~((CPU_XLEN - 1) | 0x400)) != 0)
                            goto illegal_insn;
                        if (imm & 0x400)
                            val = (cpu_int_t)s->reg[rs1] >> (imm & (CPU_XLEN - 1));
                        else
                            val = (cpu_int_t)((cpu_uint_t)s->reg[rs1] >> (imm & (CPU_XLEN - 1)));
                        break;
                    case 6: /* ori */
                        val = s->reg[rs1] | imm;
                        break;
                    default:
                    case 7: /* andi */
                        val = s->reg[rs1] & imm;
                        break;
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;

#if CPU_XLEN >= 64
        case 0x1b:/* op-imm-32 */
            funct3 = (insn >> 12) & 7;
            imm = (int32_t)insn >> 20;
            val = s->reg[rs1];
            switch(funct3) {
                case 0: /* addiw */
                    val = (int32_t)(val + imm);
                    break;
                case 1: /* slliw */
                    if ((imm & ~31) != 0)
                        goto illegal_insn;
                    val = (int32_t)(val << (imm & 31));
                    break;
                case 5: /* srliw/sraiw */
                    if ((imm & ~(31 | 0x400)) != 0)
                        goto illegal_insn;
                    if (imm & 0x400)
                        val = (int32_t)val >> (imm & 31);
                    else
                        val = (int32_t)((uint32_t)val >> (imm & 31));
                    break;
                default:
                    goto illegal_insn;
            }
            if (rd != 0)
                s->reg[rd] = val;
            break;
#endif

            case 0x33: /* op */
                imm = insn >> 25;
                val = s->reg[rs1];
                val2 = s->reg[rs2];
                if (imm == 1) {
                    funct3 = (insn >> 12) & 7;
                    switch(funct3) {
#ifdef RISCV_EXT_M
                        case 0: /* mul */
                            val = (cpu_int_t)val * (cpu_int_t)val2;
                            break;
                        case 1: /* mulh */
                            val = (cpu_int_t)mulh(val, val2);
                            break;
                        case 2:/* mulhsu */
                            val = (cpu_int_t)mulhsu(val, val2);
                            break;
                        case 3:/* mulhu */
                            val = (cpu_int_t)mulhu(val, val2);
                            break;
                        case 4:/* div */
                            val = DIV(cpu_int_t, val, val2);
                            break;
                        case 5:/* divu */
                            val = (cpu_int_t)DIVU(cpu_uint_t, val, val2);
                            break;
                        case 6:/* rem */
                            val = REM(cpu_int_t, val, val2);
                            break;
                        case 7:/* remu */
                            val = (cpu_int_t)REMU(cpu_uint_t, val, val2);
                            break;
#endif
                        default:
                            goto illegal_insn;
                    }
                } else {
                    if (imm & ~0x20)
                        goto illegal_insn;
                    funct3 = ((insn >> 12) & 7) | ((insn >> (30 - 3)) & (1 << 3));
                    switch(funct3) {
                        case 0: /* add */
                            val = (cpu_int_t)(val + val2);
                            break;
                        case 0 | 8: /* sub */
                            val = (cpu_int_t)(val - val2);
                            break;
                        case 1: /* sll */
                            val = (cpu_int_t)(val << (val2 & (CPU_XLEN - 1)));
                            break;
                        case 2: /* slt */
                            val = (cpu_int_t)val < (cpu_int_t)val2;
                            break;
                        case 3: /* sltu */
                            val = val < val2;
                            break;
                        case 4: /* xor */
                            val = val ^ val2;
                            break;
                        case 5: /* srl */
                            val = (cpu_int_t)((cpu_uint_t)val >> (val2 & (CPU_XLEN - 1)));
                            break;
                        case 5 | 8: /* sra */
                            val = (cpu_int_t)val >> (val2 & (CPU_XLEN - 1));
                            break;
                        case 6: /* or */
                            val = val | val2;
                            break;
                        case 7: /* and */
                            val = val & val2;
                            break;
                        default:
                            goto illegal_insn;
                    }
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;

#if CPU_XLEN == 64
            case 0x3b: /* op-32 */
                imm = insn >> 25;
                val = s->reg[rs1];
                val2 = s->reg[rs2];
                if (imm == 1) {
                    funct3 = (insn >> 12) & 7;
                    switch(funct3) {
                        case 0: /* mulw */
                            val = (int32_t)((int32_t)val * (int32_t)val2);
                            break;
                        case 4:/* divw */
                            val = DIV(int32_t, val, val2);
                            break;
                        case 5:/* divuw */
                            val = (int32_t)DIVU(uint32_t, val, val2);
                            break;
                        case 6:/* remw */
                            val = REM(int32_t, val, val2);
                            break;
                        case 7:/* remuw */
                            val = (int32_t)REMU(uint32_t, val, val2);
                            break;
                        default:
                            goto illegal_insn;
                    }
                } else {
                    if (imm & ~0x20)
                        goto illegal_insn;
                    funct3 = ((insn >> 12) & 7) | ((insn >> (30 - 3)) & (1 << 3));
                    switch(funct3) {
                        case 0: /* addw */
                            val = (int32_t)(val + val2);
                            break;
                        case 0 | 8: /* subw */
                            val = (int32_t)(val - val2);
                            break;
                        case 1: /* sllw */
                            val = (int32_t)((uint32_t)val << (val2 & 31));
                            break;
                        case 5: /* srlw */
                            val = (int32_t)((uint32_t)val >> (val2 & 31));
                            break;
                        case 5 | 8: /* sraw */
                            val = (int32_t)val >> (val2 & 31);
                            break;
                        default:
                            goto illegal_insn;
                    }
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;
#endif

            case 0x73: /* system */
                funct3 = (insn >> 12) & 7;
                imm = insn >> 20;   /* This is func12 */
                if (funct3 & 4)
                    val = rs1;
                else
                    val = s->reg[rs1];
                funct3 &= 3;
                switch(funct3) {
                    case 0:
                        switch(imm) {
                            case 0x000: /* ecall */
                                if (insn & 0x000fff80)
                                    goto illegal_insn;
                                raise_exception(s, CAUSE_USER_ECALL + s->priv);
                                goto done_interp;
                            case 0x001: /* ebreak */
                                if (insn & 0x000fff80)
                                    goto illegal_insn;
                                raise_exception(s, CAUSE_BREAKPOINT);
                                goto done_interp;
                            case 0x102: /* sret */
                                {
                                    if (insn & 0x000fff80)
                                        goto illegal_insn;
                                    if (s->priv < PRV_S)
                                        goto illegal_insn;
                                    handle_sret(s);
                                    goto done_interp;
                                }
                                break;
                            case 0x302: /* mret */
                                {
                                    if (insn & 0x000fff80)
                                        goto illegal_insn;
                                    if (s->priv < PRV_M)
                                        goto illegal_insn;
                                    handle_mret(s);
                                    goto done_interp;
                                }
                                break;
                            case 0x105: /* wfi */
                                if (insn & 0x00007f80)
                                    goto illegal_insn;
                                if (s->priv == PRV_U)
                                    goto illegal_insn;
                                /* stall if no enabled interrupts are pending */
                                if ((s->mip & s->mie) == 0) {
                                    s->is_stalled = true;
                                    s->pc = pc_next;
                                    goto done_interp;
                                }
                                break;
                            default:
                                /* Catch specific case of sfence.vma */
                                if ((imm >> 5) == 0x09) {
                                    /* sfence.vma */
                                    if (insn & 0x00007f80)
                                        goto illegal_insn;
                                    if (s->priv == PRV_U)
                                        goto illegal_insn;
                                    if (rs1 == 0) {
                                        tlb_flush_all(s);
                                    } else {
                                        tlb_flush_vaddr(s, s->reg[rs1]);
                                    }
                                    break;
                                }
                                goto illegal_insn;
                        }
                        break;
#ifdef RISCV_EXT_Zicsr
                    case 1: /* csrrw */
                        if (csr_read(s, &val2, imm, true))
                            goto illegal_insn;
                        val2 = (cpu_int_t)val2;
                        err = csr_write(s, imm, val);
                        if (err < 0)
                            goto illegal_insn;
                        if (rd != 0)
                            s->reg[rd] = val2;
                        if (err > 0) {
                            s->pc = pc_next;
                            goto done_interp;
                        }
                        break;
                    case 2: /* csrrs */
                    case 3: /* csrrc */
                        if (csr_read(s, &val2, imm, (rs1 != 0)))
                            goto illegal_insn;
                        val2 = (cpu_int_t)val2;
                        if (rs1 != 0) {
                            if (funct3 == 2)
                                val = val2 | val;
                            else
                                val = val2 & ~val;
                            err = csr_write(s, imm, val);
                            if (err < 0)
                                goto illegal_insn;
                        } else {
                            err = 0;
                        }
                        if (rd != 0)
                            s->reg[rd] = val2;
                        if (err > 0) {
                            s->pc = pc_next;
                            goto done_interp;
                        }
                        break;
#endif
                    default:
                        goto illegal_insn;
                }
                break;

            case 0x0f: /* misc-mem */
                funct3 = (insn >> 12) & 7;
                switch(funct3) {
                    case 0: /* fence */
                        if (insn & 0xf00fff80)
                            goto illegal_insn;
                        break;
#ifdef RISCV_EXT_Zifencei
                    case 1: /* fence.i */
                        if (insn != 0x0000100f)
                            goto illegal_insn;
                        break;
#endif
                    default:
                        goto illegal_insn;
                }
                break;

            case 0x2f: /* amo */
                funct3 = (insn >> 12) & 7;
                switch(funct3) {
#ifdef RISCV_EXT_A
#define OP_A(N)                                                                      \
                {                                                                    \
                    uint ## N ##_t rval;                                             \
                                                                                     \
                    addr = s->reg[rs1];                                              \
                    funct3 = insn >> 27;                                             \
                    switch(funct3) {                                                 \
                        case 2: /* lr.w */                                           \
                            if (rs2 != 0)                                            \
                                goto illegal_insn;                                   \
                            if (mem_read_u ## N(s, &rval, addr))                     \
                                goto mmu_exception;                                  \
                            val = (int## N ## _t)rval;                               \
                            s->load_res = addr;                                      \
                            break;                                                   \
                        case 3: /* sc.w */                                           \
                            if (s->load_res == addr) {                               \
                                if (mem_write_u ## N(s, addr, s->reg[rs2]))          \
                                goto mmu_exception;                                  \
                                val = 0;                                             \
                            } else {                                                 \
                                val = 1;                                             \
                            }                                                        \
                            break;                                                   \
                        case 1: /* amiswap.w */                                      \
                        case 0: /* amoadd.w */                                       \
                        case 4: /* amoxor.w */                                       \
                        case 0xc: /* amoand.w */                                     \
                        case 0x8: /* amoor.w */                                      \
                        case 0x10: /* amomin.w */                                    \
                        case 0x14: /* amomax.w */                                    \
                        case 0x18: /* amominu.w */                                   \
                        case 0x1c: /* amomaxu.w */                                   \
                            if (mem_read_u ## N(s, &rval, addr))                     \
                                goto mmu_exception;                                  \
                            val = (int## N ## _t)rval;                               \
                            val2 = s->reg[rs2];                                      \
                            switch(funct3) {                                         \
                                case 1: /* amiswap.w */                              \
                                    break;                                           \
                                case 0: /* amoadd.w */                               \
                                    val2 = (int## N ## _t)(val + val2);              \
                                    break;                                           \
                                case 4: /* amoxor.w */                               \
                                    val2 = (int## N ## _t)(val ^ val2);              \
                                    break;                                           \
                                case 0xc: /* amoand.w */                             \
                                    val2 = (int## N ## _t)(val & val2);              \
                                    break;                                           \
                                case 0x8: /* amoor.w */                              \
                                    val2 = (int## N ## _t)(val | val2);              \
                                    break;                                           \
                                case 0x10: /* amomin.w */                            \
                                    if ((int## N ## _t)val < (int## N ## _t)val2)    \
                                        val2 = (int## N ## _t)val;                   \
                                    break;                                           \
                                case 0x14: /* amomax.w */                            \
                                    if ((int## N ## _t)val > (int## N ## _t)val2)    \
                                        val2 = (int## N ## _t)val;                   \
                                    break;                                           \
                                case 0x18: /* amominu.w */                           \
                                    if ((uint## N ## _t)val < (uint## N ## _t)val2)  \
                                        val2 = (int## N ## _t)val;                   \
                                    break;                                           \
                                case 0x1c: /* amomaxu.w */                           \
                                    if ((uint## N ## _t)val > (uint## N ## _t)val2)  \
                                        val2 = (int## N ## _t)val;                   \
                                    break;                                           \
                                default:                                             \
                                    goto illegal_insn;                               \
                            }                                                        \
                            if (mem_write_u ## N(s, addr, val2))                     \
                                goto mmu_exception;                                  \
                            break;                                                   \
                        default:                                                     \
                            goto illegal_insn;                                       \
                    }                                                                \
                }

                    case 0x2:
                        OP_A(32);
                        break;
#if CPU_XLEN == 64
                    case 0x3:
                        OP_A(64);
                        break;
#endif
#endif
                    default:
                        goto illegal_insn;
                }
                if (rd != 0)
                    s->reg[rd] = val;
                break;

#ifdef RISCV_EXT_FD
            case 0x07: /* load-fp */
                if (s->fs == 0)
                    goto illegal_insn;
                funct3 = (insn >> 12) & 7;
                imm = (int32_t)insn >> 20;
                addr = s->reg[rs1] + imm;
                switch(funct3) {
                    case 2: /* flw */
                        {
                            uint32_t rval;
                            if (mem_read_u32(s, &rval, addr))
                                goto mmu_exception;
                            s->fp_reg[rd] = rval;
                        }
                        break;
                    case 3: /* fld */
                        {
                            uint64_t rval;
                            if(mem_read_u64(s, &rval, addr))
                                goto mmu_exception;
                            s->fp_reg[rd] = rval;
                        }
                        break;
                    default:
                        goto illegal_insn;
                }
                s->fs = 3;
                break;

            case 0x27: /* store-fp */
                if (s->fs == 0)
                    goto illegal_insn;
                funct3 = (insn >> 12) & 7;
                imm = rd | ((insn >> (25 - 5)) & 0xfe0);
                imm = (imm << 20) >> 20;
                addr = s->reg[rs1] + imm;
                switch(funct3) {
                    case 2: /* fsw */
                        if (mem_write_u32(s, addr, s->fp_reg[rs2]))
                            goto mmu_exception;
                        break;
                    case 3: /* fsd */
                        if (mem_write_u64(s, addr, s->fp_reg[rs2]))
                            goto mmu_exception;
                        break;
                    default:
                        goto illegal_insn;
                }
                break;

            case 0x43: /* madd */
                if (s->fs == 0)
                    goto illegal_insn;
                funct3 = (insn >> 25) & 3;
                rs3 = insn >> 27;
                rm = get_insn_rm(s, (insn >> 12) & 7);
                if (rm < 0)
                    goto illegal_insn;
                switch(funct3) {
                    case 0: /* fmadd.s */
                        s->fp_reg[rd] = fma_sf32(s->fp_reg[rs1], s->fp_reg[rs2],
                                                 s->fp_reg[rs3], rm, &s->fflags);
                        break;
                    case 1: /* fmadd.d */
                        s->fp_reg[rd] = fma_sf64(s->fp_reg[rs1], s->fp_reg[rs2],
                                                 s->fp_reg[rs3], rm, &s->fflags);
                        break;
                    default:
                        goto illegal_insn;
                }
                s->fs = 3;
                break;

            case 0x47: /* msub */
                if (s->fs == 0)
                    goto illegal_insn;
                funct3 = (insn >> 25) & 3;
                rs3 = insn >> 27;
                rm = get_insn_rm(s, (insn >> 12) & 7);
                if (rm < 0)
                    goto illegal_insn;
                switch(funct3) {
                    case 0: /* fmsub.s */
                        s->fp_reg[rd] = fma_sf32(s->fp_reg[rs1],
                                                 s->fp_reg[rs2],
                                                 s->fp_reg[rs3] ^ FSIGN_MASK32,
                                                 rm, &s->fflags);
                        break;
                    case 1: /* fmsub.d */
                        s->fp_reg[rd] = fma_sf64(s->fp_reg[rs1],
                                                 s->fp_reg[rs2],
                                                 s->fp_reg[rs3] ^ FSIGN_MASK64,
                                                 rm, &s->fflags);
                        break;
                    default:
                        goto illegal_insn;
                }
                s->fs = 3;
                break;

            case 0x4b: /* nmsub */
                if (s->fs == 0)
                    goto illegal_insn;
                funct3 = (insn >> 25) & 3;
                rs3 = insn >> 27;
                rm = get_insn_rm(s, (insn >> 12) & 7);
                if (rm < 0)
                    goto illegal_insn;
                switch(funct3) {
                    case 0: /* fnmsub.s */
                        s->fp_reg[rd] = fma_sf32(s->fp_reg[rs1] ^ FSIGN_MASK32,
                                                 s->fp_reg[rs2],
                                                 s->fp_reg[rs3],
                                                 rm, &s->fflags);
                        break;
                    case 1: /* fnmsub.d */
                        s->fp_reg[rd] = fma_sf64(s->fp_reg[rs1] ^ FSIGN_MASK64,
                                                 s->fp_reg[rs2],
                                                 s->fp_reg[rs3],
                                                 rm, &s->fflags);
                        break;
                    default:
                        goto illegal_insn;
                }
                s->fs = 3;
                break;

            case 0x4f: /* nmadd */
                if (s->fs == 0)
                    goto illegal_insn;
                funct3 = (insn >> 25) & 3;
                rs3 = insn >> 27;
                rm = get_insn_rm(s, (insn >> 12) & 7);
                if (rm < 0)
                    goto illegal_insn;
                switch(funct3) {
                    case 0: /* fnmadd.s */
                        s->fp_reg[rd] = fma_sf32(s->fp_reg[rs1] ^ FSIGN_MASK32,
                                                 s->fp_reg[rs2],
                                                 s->fp_reg[rs3] ^ FSIGN_MASK32,
                                                 rm, &s->fflags);
                        break;
                    case 1: /* fnmadd.d */
                        s->fp_reg[rd] = fma_sf64(s->fp_reg[rs1] ^ FSIGN_MASK64,
                                                 s->fp_reg[rs2],
                                                 s->fp_reg[rs3] ^ FSIGN_MASK64,
                                                 rm, &s->fflags);
                        break;
                    default:
                        goto illegal_insn;
                }
                s->fs = 3;
                break;

            case 0x53: /* op-fp */
                if (s->fs == 0)
                    goto illegal_insn;
                imm = insn >> 25;
                rm = (insn >> 12) & 7;
                switch(imm) {

#define F_SIZE 32
#include "cpu_float.h"

#define F_SIZE 64
#include "cpu_float.h"

                    default:
                        goto illegal_insn;
                }
                break;
#endif
            default:
illegal_insn:
            raise_exception(s, CAUSE_ILLEGAL_INSTRUCTION);
done_interp:
mmu_exception:
            s->insn_counter++;
            /* Note: we exit the interrupt loop after an exception because xlen
             * may have been changed, in which case we need to switch to the
             * other riscv_cpu_interp{32,64} function. */
            /* TODO: probably a behavior we can remove. But let's delay this
             * until it's possible to thoroughly test it. */
            goto done;
        }

        s->pc = pc_next;
        n_cycles--;
        s->insn_counter++;
    }
done: ;
}
