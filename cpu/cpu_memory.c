/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * RISC-V CPU device
 *  Memory accesses
 */

#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <cpu/cpu_private.h>
#define DBG_ITEM DBG_CPU_MEM
#include <lib/debug.h>
#include <lib/utils.h>
#include <platform/iomem.h>

static no_inline int mem_read_slow(RISCVCPUState *s,
                                   uint64_t *pval, cpu_uint_t addr,
                                   enum iomem_size_log2 size_log2);
static no_inline int mem_write_slow(RISCVCPUState *s,
                                    cpu_uint_t addr, uint64_t val,
                                    enum iomem_size_log2 size_log2);


/*
 * Memory access for different access granularities
 *
 * Trying using TLB to find translation and access RAM directly, otherwise
 * revert to slow memory accesses.
 */

#define MEM_READ(N, size_log2)                                                    \
    int mem_read_u ## N(RISCVCPUState *s, uint ## N ## _t *pval, cpu_uint_t addr) \
    {                                                                             \
        uint32_t tlb_idx = (addr >> PAGE_SHIFT) & (TLB_SIZE - 1);                 \
        TLBEntry *tlb_ent = &(s->tlb_read[tlb_idx]);                              \
        if (likely(tlb_ent->vaddr == (addr & ~(PAGE_MASK & ~((N / 8) - 1))))) {   \
            *pval = iomem_ram_read(tlb_ent->region,                               \
                                   tlb_ent->ppn + (addr & PAGE_MASK),             \
                                   size_log2);                                    \
        } else {                                                                  \
            uint64_t val;                                                         \
            int ret;                                                              \
            ret = mem_read_slow(s, &val, addr, size_log2);                        \
            if (ret)                                                              \
                return ret;                                                       \
            *pval = val;                                                          \
        }                                                                         \
        return 0;                                                                 \
    }

MEM_READ(8, 0)
MEM_READ(16, 1)
MEM_READ(32, 2)
MEM_READ(64, 3)

#define MEM_WRITE(N, size_log2)                                                  \
    int mem_write_u ## N(RISCVCPUState *s, cpu_uint_t addr, uint ## N ## _t val) \
    {                                                                            \
        uint32_t tlb_idx = (addr >> PAGE_SHIFT) & (TLB_SIZE - 1);                \
        TLBEntry *tlb_ent = &(s->tlb_write[tlb_idx]);                            \
        if (likely(tlb_ent->vaddr == (addr & ~(PAGE_MASK & ~((N / 8) - 1))))) {  \
            iomem_ram_write(tlb_ent->region, tlb_ent->ppn + (addr & PAGE_MASK),  \
                            val, size_log2);                                     \
            return 0;                                                            \
        } else {                                                                 \
            return mem_write_slow(s, addr, val, size_log2);                      \
        }                                                                        \
    }

MEM_WRITE(8, 0)
MEM_WRITE(16, 1)
MEM_WRITE(32, 2)
MEM_WRITE(64, 3)


/*
 * MMU: virtual to physical adress translation
 */

#define PTE_V_MASK (1 << 0)
#define PTE_U_MASK (1 << 4)
#define PTE_A_MASK (1 << 6)
#define PTE_D_MASK (1 << 7)

#define ACCESS_READ  0
#define ACCESS_WRITE 1
#define ACCESS_EXEC  2

/* Virtual to physical address translation */
static int get_phys_addr(RISCVCPUState *s,
                         phys_addr_t *ppaddr, cpu_uint_t vaddr,
                         int access)
{
    int mode, levels, pte_bits, pte_idx, pte_mask, xwr, priv;
    enum iomem_size_log2 pte_size_log2;
    int need_write, vaddr_shift, i, pte_addr_bits;
    cpu_uint_t pte, vaddr_mask;
    phys_addr_t pte_addr, paddr;
    iomem_t r;

    /* If MPRV is set, use previous privilege mode from MPP --but doesn't apply
     * to instruction accessess. Otherwise use current privilege mode. */
    if ((s->mstatus & MSTATUS_MPRV) && access != ACCESS_EXEC) {
        priv = (s->mstatus >> MSTATUS_MPP_SHIFT) & 3;
    } else {
        priv = s->priv;
    }

    /* No address translation in machine mode */
    if (priv == PRV_M) {
        *ppaddr = vaddr;
        return 0;
    }

    /* Get address-translation scheme */
#if CPU_XLEN == 32
    mode = s->satp >> 31;
    if (mode == 0) {
        /* bare mode: no translation */
        *ppaddr = vaddr;
        return 0;
    }

    /* sv32 */
    levels = 2;
    pte_size_log2 = IOMEM_4BYTE_SIZE_LOG2;
    pte_addr_bits = 22;
#else
    mode = (s->satp >> 60) & 0xf;

    if (mode == 0) {
        /* bare mode: no translation */
        *ppaddr = vaddr;
        return 0;
    }

    /* sv39/sv48 */
    levels = 3 + (mode - 8);
    pte_size_log2 = IOMEM_8BYTE_SIZE_LOG2;
    pte_addr_bits = 44;

    /* Upper bits (64 to 39/48) must match last relevant bit of vaddr (38/47) */
    vaddr_shift = CPU_XLEN - (PAGE_SHIFT + levels * 9);
    if ((((cpu_int_t)vaddr << vaddr_shift) >> vaddr_shift) != vaddr)
        return -1;
#endif

    /* Physical address of root page table */
    pte_addr = ((phys_addr_t)s->satp & (((cpu_uint_t)1 << pte_addr_bits) - 1))
        << PAGE_SHIFT;

    pte_bits = 12 - pte_size_log2;
    pte_mask = (1 << pte_bits) - 1;

    /* Walk through page table */
    for(i = 0; i < levels; i++) {
        /* Extract VPN[i] out of vaddr and index current page table level */
        vaddr_shift = PAGE_SHIFT + pte_bits * (levels - 1 - i);
        pte_idx = (vaddr >> vaddr_shift) & pte_mask;

        /* Access corresponding PTE */
        pte_addr += pte_idx << pte_size_log2;
        r = iomem_get_region(pte_addr);
        pte = iomem_ram_read(r, pte_addr, pte_size_log2);
        dbg_more("pte=0x%" PRIxcpu "\n", pte);

        /* Check PTE */
        if (!(pte & PTE_V_MASK))
            return -1; /* invalid PTE */

        /* Extract PPN */
        paddr = ((phys_addr_t)pte >> 10) << PAGE_SHIFT;

        /* Extract RWX bits */
        xwr = (pte >> 1) & 7;
        if (xwr != 0) {
            /* PTE is a leaf */

            /* A page cannot be write-only or write-execute */
            if (xwr == 2 || xwr == 6)
                return -1;

            /* Privilege check */
            if (priv == PRV_U && !(pte & PTE_U_MASK))
                /* Page non accessible to user mode */
                return -1;
            else if (priv == PRV_S && (pte & PTE_U_MASK)
                     && (!(s->mstatus & MSTATUS_SUM) || (access == ACCESS_EXEC)))
                /* User page not accessible to supervisor mode if SUM bit is
                 * unset or if page is executable */
                return -1;

            /* If MXR is set, execute-only pages are readable as well*/
            if (s->mstatus & MSTATUS_MXR)
                xwr |= (xwr >> 2);

            /* Protection check */
            if (((xwr >> access) & 1) == 0)
                return -1;

            /* PTE will need to be written back on first access or first write */
            need_write = !(pte & PTE_A_MASK) ||
                (!(pte & PTE_D_MASK) && access == ACCESS_WRITE);

            /* Update PTE */
            pte |= PTE_A_MASK;
            if (access == ACCESS_WRITE)
                pte |= PTE_D_MASK;

            /* Write back PTE in memory if necessary */
            if (need_write)
                iomem_ram_write(r, pte_addr, pte, pte_size_log2);

            /* Actual virtual to physical address translation */
            vaddr_mask = ((cpu_uint_t)1 << vaddr_shift) - 1;
            *ppaddr = (vaddr & vaddr_mask) | (paddr  & ~(phys_addr_t)vaddr_mask);

            dbg_more("vaddr=0x%" PRIxcpu " to paddr=0x%" PRIxpa,
                     vaddr, *ppaddr);

            return 0;
        } else {
            /* PTE is a pointer to next level of the page table */
            pte_addr = paddr;
        }
    }

    /* Incorrect page table: never found proper leaf PTE in either level */
    return -1;
}


/*
 * Slow memory access for data
 *
 * Translate address and perform memory access
 */

static no_inline int mem_read_slow(RISCVCPUState *s,
                                   uint64_t *pval, cpu_uint_t addr,
                                   enum iomem_size_log2 size_log2)
{
    int size, tlb_idx, err, align;
    phys_addr_t paddr, ret;
    iomem_t r;

    size = 1 << size_log2;
    align = addr & (size - 1);

    /* Handle unaligned accesses */
    if (align != 0) {
        switch(size_log2) {
            case IOMEM_2BYTE_SIZE_LOG2:
                {
                    uint8_t v0, v1;
                    err = mem_read_u8(s, &v0, addr);
                    if (err)
                        return err;
                    err = mem_read_u8(s, &v1, addr + 1);
                    if (err)
                        return err;
                    ret = v0 | (v1 << 8);
                }
                break;
            case IOMEM_4BYTE_SIZE_LOG2:
                {
                    uint32_t v0, v1;
                    addr -= align;
                    err = mem_read_u32(s, &v0, addr);
                    if (err)
                        return err;
                    err = mem_read_u32(s, &v1, addr + 4);
                    if (err)
                        return err;
                    ret = (v0 >> (align * 8)) | (v1 << (32 - align * 8));
                }
                break;
            case IOMEM_8BYTE_SIZE_LOG2:
                {
                    uint64_t v0, v1;
                    addr -= align;
                    err = mem_read_u64(s, &v0, addr);
                    if (err)
                        return err;
                    err = mem_read_u64(s, &v1, addr + 8);
                    if (err)
                        return err;
                    ret = (v0 >> (align * 8)) | (v1 << (64 - align * 8));
                }
                break;
            default:
                die("bug: unhandled size_log2 %d", size_log2);
        }
    } else {
        if (get_phys_addr(s, &paddr, addr, ACCESS_READ)) {
            dbg_core("%s: no valid translation for virtual address 0x%" PRIxcpu,
                     __func__, addr);
            raise_exception2(s, CAUSE_LOAD_PAGE_FAULT, addr);
            return -1;
        }
        r = iomem_get_region(paddr);
        if (!r) {
            dbg_core("%s: invalid physical address 0x%" PRIxpa,
                     __func__, paddr);
            return 0;
        } else if (iomem_region_is_ram(r)) {
            ret = iomem_ram_read(r, paddr, size_log2);

            /* Accelerate next access to the same memory page */
            tlb_idx = (addr >> PAGE_SHIFT) & (TLB_SIZE - 1);
            s->tlb_read[tlb_idx].vaddr = addr & ~PAGE_MASK;
            s->tlb_read[tlb_idx].ppn = paddr & ~PAGE_MASK;
            s->tlb_read[tlb_idx].region = r;
        } else {
            dbg_more("Read from device at physical address 0x%" PRIxpa, paddr);
            ret = iomem_dev_read(r, paddr, size_log2);
        }
    }
    *pval = ret;
    return 0;
}

static no_inline int mem_write_slow(RISCVCPUState *s,
                                    cpu_uint_t addr, uint64_t val,
                                    enum iomem_size_log2 size_log2)
{
    int size, i, tlb_idx, err;
    phys_addr_t paddr;
    iomem_t r;

    size = 1 << size_log2;

    /* Handle unaligned accesses */
    if ((addr & (size - 1)) != 0) {
        /* XXX: should avoid modifying the memory in case of exception */
        for(i = 0; i < size; i++) {
            err = mem_write_u8(s, addr + i, (val >> (8 * i)) & 0xff);
            if (err)
                return err;
        }
    } else {
        if (get_phys_addr(s, &paddr, addr, ACCESS_WRITE)) {
            dbg_core("%s: no valid translation for virtual address 0x%" PRIxcpu,
                     __func__, addr);
            raise_exception2(s, CAUSE_STORE_PAGE_FAULT, addr);
            return -1;
        }
        r = iomem_get_region(paddr);
        if (!r) {
            dbg_core("%s: invalid physical address 0x%" PRIxpa,
                     __func__, paddr);
        } else if (iomem_region_is_ram(r)) {
            iomem_ram_write(r, paddr, val, size_log2);

            /* Accelerate next access to the same memory page */
            tlb_idx = (addr >> PAGE_SHIFT) & (TLB_SIZE - 1);
            s->tlb_write[tlb_idx].vaddr = addr & ~PAGE_MASK;
            s->tlb_write[tlb_idx].ppn = paddr & ~PAGE_MASK;
            s->tlb_write[tlb_idx].region = r;
        } else {
            dbg_more("Write to device at physical address 0x%" PRIxpa, paddr);
            iomem_dev_write(r, paddr, val, size_log2);
        }
    }
    return 0;
}


/*
 * Memory access for instructions
 */

static no_inline int mem_read_insn_slow(RISCVCPUState *s,
                                           uint32_t *pinsn,
                                           cpu_uint_t addr)
{
    int tlb_idx;
    phys_addr_t paddr;
    iomem_t r;

    if (get_phys_addr(s, &paddr, addr, ACCESS_EXEC)) {
        raise_exception2(s, CAUSE_FETCH_PAGE_FAULT, addr);
        return -1;
    }

    r = iomem_get_region(paddr);
    if (!r || !iomem_region_is_ram(r)) {
        /* we only access to execute code from RAM */
        dbg_core("%s: invalid physical address 0x%" PRIxpa, __func__, paddr);
        raise_exception2(s, CAUSE_FAULT_FETCH, addr);
        return -1;
    }

    tlb_idx = (addr >> PAGE_SHIFT) & (TLB_SIZE - 1);
    s->tlb_code[tlb_idx].vaddr = addr & ~PAGE_MASK;
    s->tlb_code[tlb_idx].ppn = paddr & ~PAGE_MASK;
    s->tlb_code[tlb_idx].region = r;

    *pinsn = iomem_ram_read(r, paddr, IOMEM_4BYTE_SIZE_LOG2);

    return 0;
}

int mem_read_insn(RISCVCPUState *s, uint32_t *pinsn, cpu_uint_t addr)
{
    uint32_t tlb_idx;
    TLBEntry *tlb_ent;

    tlb_idx = (addr >> PAGE_SHIFT) & (TLB_SIZE - 1);
    tlb_ent = &(s->tlb_code[tlb_idx]);

    if (likely(tlb_ent->vaddr == (addr & ~PAGE_MASK))) {
        *pinsn = iomem_ram_read(tlb_ent->region,
                                tlb_ent->ppn + (addr & PAGE_MASK),
                                IOMEM_4BYTE_SIZE_LOG2);
        return 0;
    } else {
        return mem_read_insn_slow(s, pinsn, addr);
    }
}


/*
 * TLB management
 */

void tlb_init(RISCVCPUState *s)
{
    int i;

    for(i = 0; i < TLB_SIZE; i++) {
        s->tlb_read[i].vaddr = -1;
        s->tlb_write[i].vaddr = -1;
        s->tlb_code[i].vaddr = -1;
    }
}

void tlb_flush_all(RISCVCPUState *s)
{
    tlb_init(s);
}

void tlb_flush_vaddr(RISCVCPUState *s, cpu_uint_t vaddr)
{
    tlb_flush_all(s);
}


/*
 * Debug memory access used by GDB stub
 *
 * Arbitrary size and without modifying the TLB
 */

int mem_read_debug(RISCVCPUState *s, cpu_uint_t addr, uint8_t *buf, size_t len)
{
    while (len > 0) {
        phys_addr_t paddr;
        iomem_t r;
        uint8_t *data;
        size_t l;

        if (get_phys_addr(s, &paddr, addr, ACCESS_READ)) {
            dbg_core("%s: invalid debug virtual address 0x%" PRIxcpu,
                     __func__, addr);
            return -1;
        }

        r = iomem_get_region(paddr);
        if (!r || !iomem_region_is_ram(r)) {
            dbg_core("%s: invalid debug physical address 0x%" PRIxpa,
                     __func__, paddr);
            return -1;
        }

        /* Amount of bytes to read within current page */
        l = ((addr + PAGE_SIZE) & ~PAGE_MASK) - addr;
        if (l > len)
            l = len;

        data = iomem_ram_get_ptr(r, paddr);
        memcpy(buf, data, l);

        len -= l;
        buf += l;
        addr += l;
    }

    return 0;
}
