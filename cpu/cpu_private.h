/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * RISC-V CPU device
 *  Private API, only accessible by CPU internally
 */
#ifndef CPU_PRIVATE_H
#define CPU_PRIVATE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <cpu/cpu.h>
#include <platform/iomem.h>

#define NREGS 32

#define PRV_U 0
#define PRV_S 1
#define PRV_H 2
#define PRV_M 3

#define TLB_SIZE 256

typedef struct {
    cpu_uint_t vaddr;
    uint64_t ppn;
    iomem_t region;
} TLBEntry;

typedef struct RISCVCPUState {
    /* GP registers */
    cpu_uint_t pc;
    cpu_uint_t reg[NREGS];

    /* FP registers */
    uint64_t fp_reg[NREGS];
    uint32_t fflags;
    uint8_t frm;

    /* CSRs */
    cpu_uint_t mstatus;
    cpu_uint_t mtvec;
    cpu_uint_t mscratch;
    cpu_uint_t mepc;
    cpu_uint_t mcause;
    cpu_uint_t mtval;
    cpu_uint_t mhartid; /* ro */
    cpu_uint_t misa;
    cpu_uint_t mie;
    cpu_uint_t mip;
    cpu_uint_t medeleg;
    cpu_uint_t mideleg;
    cpu_uint_t mcounteren;

    cpu_uint_t stvec;
    cpu_uint_t sscratch;
    cpu_uint_t sepc;
    cpu_uint_t scause;
    cpu_uint_t stval;
    cpu_uint_t satp;
    cpu_uint_t scounteren;

    /* Internal registers */
    uint8_t priv; /* see PRV_x */
    uint8_t fs; /* MSTATUS_FS value */
    uint8_t mxl; /* MXL field in MISA */

    uint64_t insn_counter;
    bool is_stalled;

    cpu_uint_t load_res; /* for atomic LR/SC */

    /* TLBs */
    TLBEntry tlb_read[TLB_SIZE];
    TLBEntry tlb_write[TLB_SIZE];
    TLBEntry tlb_code[TLB_SIZE];
} RISCVCPUState;

RISCVCPUState *riscv_cpu_get_state(void);

/* misa CSR */
#if CPU_XLEN == 32
#define MISA_MXL    1
#else
#define MISA_MXL    2
#endif
#define MISA_SUPER   (1 << ('S' - 'A'))
#define MISA_USER    (1 << ('U' - 'A'))
#define MISA_I       (1 << ('I' - 'A'))
#define MISA_M       (1 << ('M' - 'A'))
#define MISA_A       (1 << ('A' - 'A'))
#define MISA_F       (1 << ('F' - 'A'))
#define MISA_D       (1 << ('D' - 'A'))
#define MISA_C       (1 << ('C' - 'A'))

/* Exception causes */
#define CAUSE_MISALIGNED_FETCH    0x0
#define CAUSE_FAULT_FETCH         0x1
#define CAUSE_ILLEGAL_INSTRUCTION 0x2
#define CAUSE_BREAKPOINT          0x3
#define CAUSE_MISALIGNED_LOAD     0x4
#define CAUSE_FAULT_LOAD          0x5
#define CAUSE_MISALIGNED_STORE    0x6
#define CAUSE_FAULT_STORE         0x7
#define CAUSE_USER_ECALL          0x8
#define CAUSE_SUPERVISOR_ECALL    0x9
#define CAUSE_HYPERVISOR_ECALL    0xa
#define CAUSE_MACHINE_ECALL       0xb
#define CAUSE_FETCH_PAGE_FAULT    0xc
#define CAUSE_LOAD_PAGE_FAULT     0xd
#define CAUSE_STORE_PAGE_FAULT    0xf

/* mip masks */
#define MIP_USIP        (1 << IRQ_U_SOFT)
#define MIP_SSIP        (1 << IRQ_S_SOFT)
#define MIP_HSIP        (1 << IRQ_H_SOFT)
#define MIP_MSIP        (1 << IRQ_M_SOFT)
#define MIP_UTIP        (1 << IRQ_U_TIMER)
#define MIP_STIP        (1 << IRQ_S_TIMER)
#define MIP_HTIP        (1 << IRQ_H_TIMER)
#define MIP_MTIP        (1 << IRQ_M_TIMER)
#define MIP_UEIP        (1 << IRQ_U_EXT)
#define MIP_SEIP        (1 << IRQ_S_EXT)
#define MIP_HEIP        (1 << IRQ_H_EXT)
#define MIP_MEIP        (1 << IRQ_M_EXT)

/* mstatus CSR */
#define MSTATUS_SPIE_SHIFT 5
#define MSTATUS_MPIE_SHIFT 7
#define MSTATUS_SPP_SHIFT  8
#define MSTATUS_MPP_SHIFT  11
#define MSTATUS_FS_SHIFT   13
#if CPU_XLEN == 64
#define MSTATUS_UXL_SHIFT  32
#define MSTATUS_SXL_SHIFT  34
#endif

#define MSTATUS_UIE  (1 << 0)
#define MSTATUS_SIE  (1 << 1)
#define MSTATUS_HIE  (1 << 2)
#define MSTATUS_MIE  (1 << 3)
#define MSTATUS_UPIE (1 << 4)
#define MSTATUS_SPIE (1 << MSTATUS_SPIE_SHIFT)
#define MSTATUS_HPIE (1 << 6)
#define MSTATUS_MPIE (1 << MSTATUS_MPIE_SHIFT)
#define MSTATUS_SPP  (1 << MSTATUS_SPP_SHIFT)
#define MSTATUS_HPP  (3 << 9)
#define MSTATUS_MPP  (3 << MSTATUS_MPP_SHIFT)
#define MSTATUS_FS   (3 << MSTATUS_FS_SHIFT)
#define MSTATUS_XS   (3 << 15)
#define MSTATUS_MPRV (1 << 17)
#define MSTATUS_SUM  (1 << 18)
#define MSTATUS_MXR  (1 << 19)
#if CPU_XLEN == 64
#define MSTATUS_UXL   ((cpu_uint_t)1 << MSTATUS_UXL_SHIFT)
#define MSTATUS_SXL   ((cpu_uint_t)1 << MSTATUS_SXL_SHIFT)
#endif

/*
 * System management
 */
void raise_exception(RISCVCPUState *s, cpu_uint_t cause);
void raise_exception2(RISCVCPUState *s, cpu_uint_t cause, cpu_uint_t tval);
void raise_interrupt(RISCVCPUState *s);

void handle_mret(RISCVCPUState *s);
void handle_sret(RISCVCPUState *s);

int csr_read(RISCVCPUState *s, cpu_uint_t *pval, uint32_t csr, bool will_write);
int csr_write(RISCVCPUState *s, uint32_t csr, cpu_uint_t val);

/*
 * Memory accesses
 */

/* Data memory accesses for various sizes */
#define MEM_READ_WRITE_PROTO(N) \
    int mem_read_u ## N(RISCVCPUState *s, uint ## N ## _t *pval, cpu_uint_t addr); \
    int mem_write_u ## N(RISCVCPUState *s, cpu_uint_t addr, uint ## N ## _t val);

MEM_READ_WRITE_PROTO(8)
MEM_READ_WRITE_PROTO(16)
MEM_READ_WRITE_PROTO(32)
MEM_READ_WRITE_PROTO(64)

/* Instruction memory read access */
int mem_read_insn(RISCVCPUState *s, uint32_t *pinsn, cpu_uint_t addr);

/* TLB management */
void tlb_init(RISCVCPUState *s);
void tlb_flush_all(RISCVCPUState *s);
void tlb_flush_vaddr(RISCVCPUState *s, cpu_uint_t vaddr);

/* Debug access (for GDB) */
int mem_read_debug(RISCVCPUState *s, cpu_uint_t addr, uint8_t *buf, size_t len);

/*
 * Instruction interpretation
 */
void riscv_cpu_interp_rv(RISCVCPUState *s, int n_cycles);

#endif /* CPU_PRIVATE_H */
