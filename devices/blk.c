/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2003-2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Lupio-BLK - Block virtual device
 */

#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cpu/cpu.h>
#include <devices/blk.h>
#include <devices/pic.h>
#define DBG_ITEM DBG_BLK
#include <lib/debug.h>
#include <lib/utils.h>
#include <platform/iomem.h>

#define SECTOR_BITS 9       /* Typical 512 bytes per disk sector */
#define SECTOR_SIZE (1UL << SECTOR_BITS)

/*
 * Backend: interface with host's disk image
 */
struct disk_img {
    FILE *f;
    int64_t nb_sectors;
    enum disk_img_mode mode;
    uint8_t **sector_table;
};

static struct disk_img disk_img;

static int64_t disk_img_sector_count(void)
{
    return disk_img.nb_sectors;
}

static int disk_img_read(uint64_t sector_num, uint8_t *buf, int n)
{
    if (!disk_img.f || (sector_num + n > disk_img.nb_sectors))
        return -1;

    switch(disk_img.mode) {
        case DISK_IMG_RO:
        case DISK_IMG_RW:
            /* Read sectors directly from disk image */
            fseek(disk_img.f, sector_num * SECTOR_SIZE, SEEK_SET);
            fread(buf, sizeof(uint8_t), n * SECTOR_SIZE, disk_img.f);
            break;

        case DISK_IMG_PV:
            {
                int i;
                for(i = sector_num; i < n + sector_num; i++) {
                    if (!disk_img.sector_table[i]) {
                        /* Unmodified sector, read from original disk image */
                        fseek(disk_img.f, i * SECTOR_SIZE, SEEK_SET);
                        fread(buf, sizeof(uint8_t), SECTOR_SIZE, disk_img.f);
                    } else {
                        /* Modified sector, read from private memory copy */
                        memcpy(buf, disk_img.sector_table[i], SECTOR_SIZE);
                    }
                    buf += SECTOR_SIZE;
                }
            }
            break;

        default:
            die("bug: unhandled disk mode %d", disk_img.mode);
    }

    return 0;
}

static int disk_img_write(uint64_t sector_num, const uint8_t *buf, int n)
{
    if (!disk_img.f || (sector_num + n > disk_img.nb_sectors))
        return -1;

    switch (disk_img.mode) {
        case DISK_IMG_RO:
            /* Cannot write anything to disk in RO mode; return error */
            return -1;
        case DISK_IMG_RW:
            /* Write sectors directly to disk image */
            fseek(disk_img.f, sector_num * SECTOR_SIZE, SEEK_SET);
            fwrite(buf, sizeof(uint8_t), n * SECTOR_SIZE, disk_img.f);
            break;
        case DISK_IMG_PV:
            {
                int i;
                /* Write each new sector's contents in memory */
                for (i = sector_num; i < n + sector_num; i++) {
                    if (!disk_img.sector_table[i])
                        disk_img.sector_table[i] = malloc(SECTOR_SIZE);
                    memcpy(disk_img.sector_table[i], buf, SECTOR_SIZE);
                    buf += SECTOR_SIZE;
                }
            }
            break;

        default:
            die("bug: unhandled disk mode %d", disk_img.mode);
    }

    return 0;
}

void disk_img_init(const char *filename, enum disk_img_mode mode)
{
    int64_t file_size;
    FILE *f;
    const char *mode_str;

    if (mode == DISK_IMG_RW)
        mode_str = "r+b";
    else
        mode_str = "rb";

    f = xfopen(filename, mode_str);

    fseek(f, 0, SEEK_END);
    file_size = ftello(f);

    disk_img.mode = mode;
    disk_img.nb_sectors = file_size / SECTOR_SIZE;
    disk_img.f = f;

    if (!is_power_of_2(disk_img.nb_sectors) || disk_img.nb_sectors > (1ULL << 32))
        die("%s: number of 512B blocks must be a power of 2,"
            " and less than or equal to 2^32", __func__);

    if (mode == DISK_IMG_PV)
        disk_img.sector_table = xmallocz(sizeof(disk_img.sector_table[0]) *
                                         disk_img.nb_sectors);
}

void disk_img_exit(void)
{
    int i;

    if (!disk_img.f)
        return;

    fclose(disk_img.f);
    if (disk_img.sector_table) {
        for (i = 0; i < disk_img.nb_sectors; i++)
            if (disk_img.sector_table[i])
                free(disk_img.sector_table[i]);
        free(disk_img.sector_table);
    }
    memset(&disk_img, 0, sizeof(disk_img));
}

/*
 * Frontend: LupIO-BLK device implementation
 */
enum {
    LUPIO_BLK_CONF        = 0x0,

    LUPIO_BLK_NBLK        = 0x4,
    LUPIO_BLK_BLKA        = 0x8,
    LUPIO_BLK_MEMA        = 0xC,

    LUPIO_BLK_CTRL        = 0x10, /* [1]: Type, [0]: IE */
    LUPIO_BLK_STAT        = 0x14, /* [2]: Error, [1]: Type, [0]: IE */

    /* Top oset in register map */
    LUPIO_BLK_OFFSET_MAX  = 0x18
};

enum {
    /* Shared between CTRL and STAT */
    LUPIO_BLK_TYPE = 0x2,

    /* Specific to CTRL */
    LUPIO_BLK_IRQE = 0x1,

    /* Specific to STAT */
    LUPIO_BLK_IDLE = 0x0,
    LUPIO_BLK_BUSY = 0x1,
    LUPIO_BLK_ERRR = 0x4,
};

struct lupio_blk_dev {
    int irq_num;
    uint32_t nblk, lba, mem;
    bool rw, err;
};

static struct lupio_blk_dev blk_dev;

static uint64_t lupio_blk_read(void *dev, phys_addr_t offset,
                               enum iomem_size_log2 size_log2)
{
    uint32_t val = 0;

    switch(offset) {
        case LUPIO_BLK_CONF:
            val = SECTOR_BITS << 16                 /* log2(512B / block) */
                | ctz32(disk_img_sector_count());   /* log2(Nb of blocks) */
            break;

        case LUPIO_BLK_NBLK:
            val = blk_dev.nblk;
            break;
        case LUPIO_BLK_BLKA:
            val = blk_dev.lba;
            break;
        case LUPIO_BLK_MEMA:
            val = blk_dev.mem;
            break;

        case LUPIO_BLK_STAT:
            val = 0;                    /* Always idle */
            if (blk_dev.rw)
                val |= LUPIO_BLK_TYPE;  /* Write command */
            if (blk_dev.err)
                val |= LUPIO_BLK_ERRR;  /* Error */

            /* Reset IRQ */
            lupio_pic_set_irq(blk_dev.irq_num, false);
            break;

        default:
            dbg_core("%s: invalid read access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }

    return val;
}

static void lupio_blk_do_request(void)
{
    iomem_t r;
    void *ptr;
    int ret;

    dbg_more("%s: lba=0x%08x mem=0x%08x nblk=0x%08x (%s)", __func__,
             blk_dev.lba, blk_dev.mem, blk_dev.nblk, (blk_dev.rw ? "write" : "read"));

    r = iomem_get_region(blk_dev.mem);
    if (!r || !iomem_region_is_ram(r)
        || (blk_dev.mem + blk_dev.nblk * SECTOR_SIZE >
            iomem_region_addr(r) + iomem_region_size(r))) {
        dbg_core("%s: invalid request parameters", __func__);
        goto err;
    }

    ptr = iomem_ram_get_ptr(r, blk_dev.mem);
    if (!blk_dev.rw)
        ret = disk_img_read(blk_dev.lba, ptr, blk_dev.nblk);
    else
        ret = disk_img_write(blk_dev.lba, ptr, blk_dev.nblk);
    if (ret)
        goto err;

    return;

err:
    blk_dev.err = true;
}

static void lupio_blk_write(void *dev, phys_addr_t offset, uint64_t val,
                            enum iomem_size_log2 size_log2)
{
    switch(offset) {
        case LUPIO_BLK_NBLK:
            blk_dev.nblk = val;
            break;
        case LUPIO_BLK_BLKA:
            blk_dev.lba = val;
            break;
        case LUPIO_BLK_MEMA:
            blk_dev.mem = val;
            break;

        case LUPIO_BLK_CTRL:
            blk_dev.err = false;
            blk_dev.rw = !!(val & LUPIO_BLK_TYPE);

            /* Perform transfer */
            lupio_blk_do_request();

            /* Raise IRQ if required */
            if (val & LUPIO_BLK_IRQE)
                lupio_pic_set_irq(blk_dev.irq_num, true);
            break;

        default:
            dbg_core("%s: invalid write access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }
}

static struct iomem_dev_ops lupio_blk_ops = {
    .read = lupio_blk_read,
    .write = lupio_blk_write,
    .flags = IOMEM_DEV_SIZE32,
};

void lupio_blk_init(phys_addr_t base, phys_addr_t size, int irq_num)
{
    assert(size >= LUPIO_BLK_OFFSET_MAX);

    iomem_register_device(base, size, NULL, &lupio_blk_ops);

    blk_dev.irq_num = irq_num;
}
