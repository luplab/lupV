/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-PIC - Programmable interrupt controller virtual device
 */

#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>

#include <cpu/cpu.h>
#include <devices/pic.h>
#define DBG_ITEM DBG_PIC
#include <lib/debug.h>
#include <lib/utils.h>
#include <platform/iomem.h>

/*
 * Frontend: LupIO-PIC virtual device implementation
 */

enum {
    LUPIO_PIC_PRIO          = 0x0,
    LUPIO_PIC_MASK          = 0x4,
    LUPIO_PIC_PEND          = 0x8,
    LUPIO_PIC_ENAB          = 0xC,

    /* Top offset in register map */
    LUPIO_PIC_OFFSET_MAX    = 0x10,
};

struct lupio_pic_dev {
    int cpu_irq;
    uint32_t pending, mask;
};

static struct lupio_pic_dev pic_dev;

static void lupio_pic_update_mip(void)
{
    uint32_t mask;
    mask = pic_dev.pending & pic_dev.mask;
    riscv_cpu_set_irq(pic_dev.cpu_irq, !!(mask));
}

static uint64_t lupio_pic_read(void *dev, phys_addr_t offset,
                               enum iomem_size_log2 size_log2)
{
    uint32_t val = 0;

    switch(offset) {
        case LUPIO_PIC_PRIO:
            /* Value will be 32 if no enabled and unmasked pending IRQ */
            val = ctz32(pic_dev.mask & pic_dev.pending);
            break;

        case LUPIO_PIC_MASK:
            val = pic_dev.mask;
            break;

        case LUPIO_PIC_PEND:
            val = pic_dev.pending;
            break;

        case LUPIO_PIC_ENAB:
            /* Unimplemented */
        default:
            dbg_core("%s: invalid read access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }

    return val;
}

static void lupio_pic_write(void *dev, phys_addr_t offset, uint64_t val,
                            enum iomem_size_log2 size_log2)
{
    switch(offset) {
        case LUPIO_PIC_MASK:
            pic_dev.mask = val;
            lupio_pic_update_mip();
            break;

        default:
            dbg_core("%s: invalid write access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }
}

static struct iomem_dev_ops lupio_pic_ops = {
    .read = lupio_pic_read,
    .write = lupio_pic_write,
    .flags = IOMEM_DEV_SIZE32,
};

void lupio_pic_init(phys_addr_t base, phys_addr_t size, int cpu_irq)
{
    assert(size >= LUPIO_PIC_OFFSET_MAX);

    iomem_register_device(base, size, NULL, &lupio_pic_ops);

    pic_dev.cpu_irq = cpu_irq;
}

void lupio_pic_set_irq(int irq_num, bool state)
{
    uint32_t irq_mask = 1UL << irq_num;

    if (state)
        /* Set IRQ */
        pic_dev.pending |= irq_mask;
    else
        /* Clear IRQ */
        pic_dev.pending &= ~irq_mask;

    lupio_pic_update_mip();
}

