/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-PIC - Programmable interrupt controller virtual device
 */
#ifndef PIC_H
#define PIC_H

#include <stdbool.h>
#include <stdint.h>

#include <cpu/cpu.h>

/*
 * LupIO-PIC device interface
 */
void lupio_pic_init(phys_addr_t base, phys_addr_t size, int cpu_irq);
void lupio_pic_set_irq(int irq_num, bool state);

#endif /* PIC_H */
