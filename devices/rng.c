/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2021 Joël Porquet-Lupine
 *
 * LupIO-RNG - Realtime clock virtual device
 */

#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>

#include <devices/rng.h>
#define DBG_ITEM DBG_RNG
#include <lib/debug.h>
#include <platform/iomem.h>

/*
 * Frontend: LupIO-RNG virtual device implementation
 */
enum {
    LUPIO_RNG_RAND          = 0x0,
    LUPIO_RNG_SEED          = 0x4,
    LUPIO_RNG_CTRL          = 0x8,
    LUPIO_RNG_STAT          = 0xC,

    /* Top offset in register map */
    LUPIO_RNG_OFFSET_MAX    = 0x10,
};

struct lupio_rng_dev {
    uint32_t seed;
};

static struct lupio_rng_dev rng_dev;

static uint64_t lupio_rng_read(void *dev, phys_addr_t offset,
                               enum iomem_size_log2 size_log2)
{
    uint32_t val = 0;

    switch(offset) {
        case LUPIO_RNG_RAND:
            val = mrand48();
            break;

        case LUPIO_RNG_SEED:
            val = rng_dev.seed;
            break;

        case LUPIO_RNG_STAT:
            val = 0;    /* Always ready */
            break;

        default:
            dbg_core("%s: invalid read access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }

    return val;
}

static void lupio_rng_write(void *dev, phys_addr_t offset, uint64_t val,
                            enum iomem_size_log2 size_log2)
{
    switch(offset) {
        case LUPIO_RNG_SEED:
            rng_dev.seed = val;
            srand48(rng_dev.seed);
            break;

        default:
            dbg_core("%s: invalid write access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }
}

static struct iomem_dev_ops lupio_rng_ops = {
    .read = lupio_rng_read,
    .write = lupio_rng_write,
    .flags = IOMEM_DEV_SIZE32,
};

void lupio_rng_init(phys_addr_t base, phys_addr_t size)
{
    assert(size >= LUPIO_RNG_OFFSET_MAX);

    iomem_register_device(base, size, NULL, &lupio_rng_ops);

    srand48(rng_dev.seed);
}
