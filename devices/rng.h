/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2021 Joël Porquet-Lupine
 *
 * LupIO-RNG - Random number generator virtual device
 */
#ifndef RNG_H
#define RNG_H

#include <stdint.h>

#include <cpu/cpu.h>

/*
 * LupIO-RNG device interface
 */
void lupio_rng_init(phys_addr_t base, phys_addr_t size);

#endif /* RNG_H */
