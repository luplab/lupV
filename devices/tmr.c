/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-TMR - Timer virtual device
 */

#include <assert.h>
#include <inttypes.h>
#include <time.h>

#include <cpu/cpu.h>
#include <devices/tmr.h>
#define DBG_ITEM DBG_TMR
#include <lib/debug.h>
#include <platform/iomem.h>

/*
 * Backend: interface with host
 */
#define NSEC_PER_SEC    1000000000UL    /* Number of nanoseconds per second */
#define MSEC_PER_SEC    1000            /* Number of milliseconds per second */

/* The return value is counted in terms of clock periods (100 ns at 10 MHz) */
static uint64_t get_clock_time(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (uint64_t)ts.tv_sec * TIMEBASE_FREQ +
        (ts.tv_nsec / (NSEC_PER_SEC / TIMEBASE_FREQ));
}


/*
 * Frontend: LupIO-TMR virtual device implementation
 */
enum {
    LUPIO_TMR_TIME         = 0x0,
    LUPIO_TMR_LOAD         = 0x4,
    LUPIO_TMR_CTRL         = 0x8, /* [1]: Periodic, [0]: IRQ */
    LUPIO_TMR_STAT         = 0xC, /* [0]: Expired */

    /* Top offset in register map */
    LUPIO_TMR_OFFSET_MAX   = 0x10,
};

enum {
    /* Specific to CTRL */
    LUPIO_TMR_IRQE  = 0x1,
    LUPIO_TMR_PRDC  = 0x2,

    /* Specific to STAT */
    LUPIO_TMR_EXPD  = 0x1,
};

struct lupio_tmr_dev {
    int cpu_irq;
    uint64_t reset_time;
    uint64_t event_time;
    uint32_t reload;
    bool ie, pd;
    bool expired;
};

static struct lupio_tmr_dev tmr_dev;

static uint64_t lupio_tmr_current_time(void)
{
    return get_clock_time() - tmr_dev.reset_time;
}

static uint64_t lupio_tmr_read(void *dev, phys_addr_t offset,
                           enum iomem_size_log2 size_log2)
{
    uint32_t val = 0;

    switch(offset) {
        case LUPIO_TMR_TIME:
            val = lupio_tmr_current_time();
            break;

        case LUPIO_TMR_LOAD:
            val = tmr_dev.reload;
            break;

        case LUPIO_TMR_STAT:
            if (tmr_dev.expired)
                val |= LUPIO_TMR_EXPD;

            /* Acknowledge expiration */
            tmr_dev.expired = false;
            riscv_cpu_set_irq(tmr_dev.cpu_irq, false);
            break;

        default:
            dbg_core("%s: invalid read access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }
    return val;
}

static void lupio_tmr_write(void *dev, phys_addr_t offset, uint64_t val,
                        enum iomem_size_log2 size_log2)
{
    switch(offset) {
        case LUPIO_TMR_LOAD:
            tmr_dev.reload = val;
            break;

        case LUPIO_TMR_CTRL:
            tmr_dev.ie = !!(val & LUPIO_TMR_IRQE);
            tmr_dev.pd = !!(val & LUPIO_TMR_PRDC);

            /* Stop current timer */
            tmr_dev.event_time = 0;

            /* Start new timer if reload value is not zero */
            if (tmr_dev.reload)
                tmr_dev.event_time = lupio_tmr_current_time() + tmr_dev.reload;
            break;

        default:
            dbg_core("%s: invalid write access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }
}

static struct iomem_dev_ops lupio_tmr_ops = {
    .read = lupio_tmr_read,
    .write = lupio_tmr_write,
    .flags = IOMEM_DEV_SIZE32,
};

void lupio_tmr_init(phys_addr_t base, phys_addr_t size, int cpu_irq)
{
    assert(size >= LUPIO_TMR_OFFSET_MAX);

    iomem_register_device(base, size, NULL, &lupio_tmr_ops);

    tmr_dev.reset_time = get_clock_time();
    tmr_dev.event_time = 0;
    tmr_dev.cpu_irq = cpu_irq;
}

int lupio_tmr_expiration(void)
{
    int32_t delay;

    /* No timer currently running */
    if (!tmr_dev.event_time)
        return -1;

    /* Delay until timer expires */
    delay = tmr_dev.event_time - lupio_tmr_current_time();
    if (delay <= 0) {
        /* Signal expiration */
        tmr_dev.expired = true;
        if (tmr_dev.ie)
            riscv_cpu_set_irq(tmr_dev.cpu_irq, true);

        /* If periodic timer, reload */
        if (tmr_dev.pd && tmr_dev.reload)
            tmr_dev.event_time = lupio_tmr_current_time()
                + tmr_dev.reload - delay;
        else
            tmr_dev.event_time = 0;

        return 0;
    }

    /* Convert clock periods to actual milliseconds */
    return delay / (TIMEBASE_FREQ / MSEC_PER_SEC);
}

