/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-TTY - Terminal virtual device
 */

#include <assert.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include <devices/pic.h>
#include <devices/tty.h>
#define DBG_ITEM DBG_TTY
#include <lib/debug.h>
#include <platform/iomem.h>

/*
 * Backend: interface with host's terminal
 */
static struct termios saved_termios;
static int saved_stdin_flags;

void stdio_exit(void)
{
    /* Restore backups for termios and status flags */
    tcsetattr(STDIN_FILENO, TCSANOW, &saved_termios);
    fcntl(STDIN_FILENO, F_SETFL, saved_stdin_flags);
}

void stdio_init(bool nsignals)
{
    struct termios termios;

    /* Save copy of current termios parameters */
    memset(&termios, 0, sizeof(termios));
    tcgetattr (STDIN_FILENO, &termios);
    saved_termios = termios;

    /* Save copy of current stdin's status flags */
    saved_stdin_flags = fcntl(STDIN_FILENO, F_GETFL);

    /* Reconfigure termios parameters */
    termios.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP
                     |INLCR|IGNCR|ICRNL|IXON);
    termios.c_oflag |= OPOST;
    termios.c_lflag &= ~(ECHO|ECHONL|ICANON|IEXTEN);
    if (nsignals)
        termios.c_lflag &= ~ISIG;
    termios.c_cflag &= ~(CSIZE|PARENB);
    termios.c_cflag |= CS8;
    termios.c_cc[VMIN] = 1;
    termios.c_cc[VTIME] = 0;
    tcsetattr(STDIN_FILENO, TCSANOW, &termios);

    /* Make stdin be non-blocking */
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

static void stdio_putc(const char c)
{
    putc(c, stdout);
    fflush(stdout);
}

static int stdio_getc(char *c)
{
    int ret;
    uint8_t ch;

    static int stdio_esc_state;

    ret = read(0, &ch, 1);
    if (ret < 0)
        return 0;
    if (ret == 0) {
        /* EOF */
        exit(1);
    }

    if (stdio_esc_state) {
        stdio_esc_state = 0;
        switch(ch) {
            case 'x':
                printf("Terminated\n");
                exit(0);
            case 'h':
                printf("\n"
                       "C-a h   print this help\n"
                       "C-a x   exit emulator\n"
                       "C-a C-a send C-a\n");
                break;
            case 1:
                goto output_char;
            default:
                break;
        }
        ret = 0;
    } else {
        if (ch == 1) {
            dbg_more("%s: read escape character", __func__);
            stdio_esc_state = 1;
            ret = 0;
        } else {
output_char:
            dbg_more("%s: read character %c", __func__, ch);
            *c = ch;
            ret = 1;
        }
    }
    return ret;
}

/*
 * Frontend: LupIO-TTY virtual device implementation
 */
enum {
    LUPIO_TTY_WRIT         = 0x0,
    LUPIO_TTY_READ         = 0x4,
    LUPIO_TTY_CTRL         = 0x8, /* [1]: Read IE, [0]: Write IE */
    LUPIO_TTY_STAT         = 0xC, /* [1]: Read ready, [0]: Write ready */

    /* Top offset in register map */
    LUPIO_TTY_OFFSET_MAX   = 0x10,
};

enum {
    LUPIO_TTY_WBIT  = 0x1,
    LUPIO_TTY_RBIT  = 0x2,

    LUPIO_TTY_INVAL = 0x80000000,
};

struct lupio_tty_dev {
    int writ_char, read_char;
    int irq_num;
    bool writ_ie, read_ie;
};

static struct lupio_tty_dev tty_dev;

static void lupio_tty_update_irq(void)
{
    if ((tty_dev.writ_ie && tty_dev.writ_char != -1)
        || (tty_dev.read_ie && tty_dev.read_char != -1))
        lupio_pic_set_irq(tty_dev.irq_num, true);
    else
        lupio_pic_set_irq(tty_dev.irq_num, false);
}

static uint64_t lupio_tty_read(void *dev, phys_addr_t offset,
                                    enum iomem_size_log2 size_log2)
{
    uint32_t val = LUPIO_TTY_INVAL;

    switch(offset) {
        case LUPIO_TTY_WRIT:
            /* Reading written character lowers IRQ */
            if (tty_dev.writ_char != -1) {
                val = tty_dev.writ_char & 0xFF;
                tty_dev.writ_char = -1;
                lupio_tty_update_irq();
            }
            break;

        case LUPIO_TTY_READ:
            if (tty_dev.read_char != -1) {
                /* Return new character */
                val = tty_dev.read_char & 0xFF;
                tty_dev.read_char = -1;
                lupio_tty_update_irq();
            }
            break;

        case LUPIO_TTY_CTRL:
            val = 0;
            if (tty_dev.writ_ie)
                val |= LUPIO_TTY_WBIT;
            if (tty_dev.read_ie)
                val |= LUPIO_TTY_RBIT;
            break;

        case LUPIO_TTY_STAT:
            /* Always ready to write */
            val = LUPIO_TTY_WBIT;
            if (tty_dev.read_char != -1)
                val |= LUPIO_TTY_RBIT;
            break;

        default:
            dbg_core("%s: invalid read access at offset=0x%" PRIxpa,
                     __func__, offset);
            val = 0;
            break;
    }

    return val;
}

static void lupio_tty_write(void *dev, phys_addr_t offset, uint64_t val,
                                 enum iomem_size_log2 size_log2)
{
    switch(offset) {
        case LUPIO_TTY_WRIT:
            tty_dev.writ_char = val & 0xFF;
            stdio_putc(tty_dev.writ_char);
            lupio_tty_update_irq();
            break;

        case LUPIO_TTY_CTRL:
            tty_dev.writ_ie = !!(val & LUPIO_TTY_WBIT);
            tty_dev.read_ie = !!(val & LUPIO_TTY_RBIT);
            lupio_tty_update_irq();
            break;

        default:
            dbg_core("%s: invalid write access at offset=0x%" PRIxpa,
                     __func__, offset);
            break;
    }
}

static struct iomem_dev_ops lupio_tty_ops = {
    .read = lupio_tty_read,
    .write = lupio_tty_write,
    .flags = IOMEM_DEV_SIZE32,
};

void lupio_tty_init(phys_addr_t base, phys_addr_t size, int irq_num)
{
    assert(size >= LUPIO_TTY_OFFSET_MAX);

    iomem_register_device(base, size, NULL, &lupio_tty_ops);

    tty_dev.writ_char = tty_dev.read_char = -1;
    tty_dev.irq_num = irq_num;
}

bool lupio_tty_receive_ready(void)
{
    return (tty_dev.read_char == -1);
}

void lupio_tty_receive_char(void)
{
    char c;
    int ret;

    /* Get character from terminal backend */
    ret = stdio_getc(&c);
    if (ret == 1) {
        /* Receive character in terminal device */
        tty_dev.read_char = c;
        /* Raise IRQ if enabled */
        lupio_tty_update_irq();
    }
}

