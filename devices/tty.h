/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * LupIO-TTY - Terminal virtual device
 */
#ifndef TTY_H
#define TTY_H

#include <stdbool.h>
#include <stdint.h>

#include <cpu/cpu.h>

/*
 * Backend using host's terminal
 */
void stdio_init(bool nsignals);
void stdio_exit(void);

/*
 * LupIO-TTY device interface
 */
void lupio_tty_init(phys_addr_t base, phys_addr_t size, int irq_num);
bool lupio_tty_receive_ready(void);
void lupio_tty_receive_char(void);

#endif /* TTY_H */
