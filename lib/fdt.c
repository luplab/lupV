/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016-2017 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Flattened device tree (FDT) generator
 */

#include <assert.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <lib/byteorder.h>
#include <lib/fdt.h>
#include <lib/utils.h>

/*
 * Format definition
 */
#define FDT_MAGIC   0xd00dfeed
#define FDT_VERSION 17

#define FDT_BEGIN_NODE  1
#define FDT_END_NODE    2
#define FDT_PROP        3
#define FDT_END         9

struct fdt_header {
    uint32_t magic;
    uint32_t totalsize;
    uint32_t off_dt_struct;
    uint32_t off_dt_strings;
    uint32_t off_mem_rsvmap;
    uint32_t version;
    uint32_t last_comp_version; /* <= 17 */
    uint32_t boot_cpuid_phys;
    uint32_t size_dt_strings;
    uint32_t size_dt_struct;
};

struct fdt_reserve_entry {
    uint64_t address;
    uint64_t size;
};

struct fdt {
    uint32_t *dt_struct;
    int dt_struct_len;
    int dt_struct_size;

    char *dt_strings;
    int dt_strings_len;
    int dt_strings_size;

    int open_node_count;
};

/*
 * Creation/destruction
 */
fdt_t fdt_create(void)
{
    struct fdt *fdt;
    fdt = xmallocz(sizeof(*fdt));
    return fdt;
}

void fdt_destroy(fdt_t fdt)
{
    free(fdt->dt_struct);
    free(fdt->dt_strings);
    free(fdt);
}

/*
 * Low-level internal writing
 */
#define FDT_ALLOC_LEN(buf, len)                                     \
do {                                                                \
    if (unlikely((len) > buf ## _size)) {                           \
        int new_size = max((len), buf ## _size * 3 / 2);            \
        buf = realloc((buf), new_size * sizeof(typeof(buf[0])));    \
        buf ## _size = new_size;                                    \
    }                                                               \
} while (0)

static void fdt_put_u32(fdt_t fdt, uint32_t v)
{
    FDT_ALLOC_LEN(fdt->dt_struct, fdt->dt_struct_len + 1);

    /* All values in FDT are in big endian format, hence the automatic swap */
    fdt->dt_struct[fdt->dt_struct_len++] = bswap_32(v);
}

static void fdt_put_buf(fdt_t fdt, const uint8_t *buf, int len)
{
    int len1;

    /* Enfore alignment on 4 bytes */
    len1 = (len + 3) / 4;
    FDT_ALLOC_LEN(fdt->dt_struct, fdt->dt_struct_len + len1);

    /* Copy buffer, and pad remaining bytes with zeros if any */
    memcpy(fdt->dt_struct + fdt->dt_struct_len, buf, len);
    memset((uint8_t *)(fdt->dt_struct + fdt->dt_struct_len) + len, 0, -len & 3);
    fdt->dt_struct_len += len1;
}

static int fdt_put_prop(fdt_t fdt, const char *prop_name)
{
    int pos, str_len;

    /* If @prop_name already exists, return its pos */
    pos = 0;
    while (pos < fdt->dt_strings_len) {
        if (!strcmp(fdt->dt_strings + pos, prop_name))
            return pos;
        pos += strlen(fdt->dt_strings + pos) + 1;
    }

    /* Otherwise, add new string to list */
    str_len = strlen(prop_name) + 1;
    FDT_ALLOC_LEN(fdt->dt_strings, fdt->dt_strings_len + str_len);

    pos = fdt->dt_strings_len;
    memcpy(fdt->dt_strings + pos, prop_name, str_len);
    fdt->dt_strings_len += str_len;

    return pos;
}

/*
 * Nodes
 */
void fdt_begin_node(fdt_t fdt, const char *name)
{
    fdt_put_u32(fdt, FDT_BEGIN_NODE);
    fdt_put_buf(fdt, (uint8_t *)name, strlen(name) + 1);
    fdt->open_node_count++;
}

void fdt_begin_node_num(fdt_t fdt, const char *name, uint32_t n)
{
    char buf[256];
    snprintf(buf, sizeof(buf), "%s@%" PRIx32, name, n);
    fdt_begin_node(fdt, buf);
}

void fdt_end_node(fdt_t fdt)
{
    fdt_put_u32(fdt, FDT_END_NODE);
    fdt->open_node_count--;
}

/*
 * Properties
 */
static void fdt_prop_base(fdt_t fdt, const char *prop_name, int size)
{
    fdt_put_u32(fdt, FDT_PROP);
    fdt_put_u32(fdt, size);
    fdt_put_u32(fdt, fdt_put_prop(fdt, prop_name));
}

void fdt_prop_blk(fdt_t fdt, const char *prop_name)
{
    fdt_prop_base(fdt, prop_name, 0);
}

void fdt_prop_u32(fdt_t fdt, const char *prop_name, uint32_t val)
{
    fdt_prop_base(fdt, prop_name, sizeof(val));
    fdt_put_u32(fdt, val);
}

void fdt_prop_u32n(fdt_t fdt, const char *prop_name, int n, ...)
{
    va_list ap;
    int i;

    fdt_prop_base(fdt, prop_name, n * sizeof(uint32_t));

    va_start(ap, n);
    for(i = 0; i < n; i++) {
        uint32_t val = va_arg(ap, uint32_t);
        fdt_put_u32(fdt, val);
    }
    va_end(ap);
}

void fdt_prop_u64(fdt_t fdt, const char *prop_name, uint64_t val)
{
    fdt_prop_base(fdt, prop_name, sizeof(val));
    fdt_put_u32(fdt, val >> 32);
    fdt_put_u32(fdt, val);
}

void fdt_prop_u64n(fdt_t fdt, const char *prop_name, int n, ...)
{
    va_list ap;
    int i;

    fdt_prop_base(fdt, prop_name, n * sizeof(uint64_t));

    va_start(ap, n);
    for(i = 0; i < n; i++) {
        uint64_t val = va_arg(ap, uint64_t);
        fdt_put_u32(fdt, val >> 32);
        fdt_put_u32(fdt, val);
    }
    va_end(ap);
}

void fdt_prop_str(fdt_t fdt, const char *prop_name, const char *str)
{
    fdt_prop_base(fdt, prop_name, strlen(str) + 1);
    fdt_put_buf(fdt, (uint8_t*)str, strlen(str) + 1);
}

void fdt_prop_strn(fdt_t fdt, const char *prop_name, int n, ...)
{
    va_list ap;
    int i, size, str_size;
    char *str, *tab;

    /* Determine overall size of all strings */
    va_start(ap, n);
    size = 0;
    for(i = 0; i < n; i++) {
        str = va_arg(ap, char *);
        assert(str);
        str_size = strlen(str) + 1;
        size += str_size;
    }
    va_end(ap);

    /* Allocate enough space to concatenate all the strings */
    tab = malloc(size);

    /* Concatenate all the strings into allocated space */
    va_start(ap, n);
    size = 0;
    for(i = 0; i < n; i++) {
        str = va_arg(ap, char *);
        str_size = strlen(str) + 1;
        memcpy(tab + size, str, str_size);
        size += str_size;
    }
    va_end(ap);

    /* Write property as a raw buffer */
    fdt_prop_base(fdt, prop_name, size);
    fdt_put_buf(fdt, (uint8_t*)tab, size);
    free(tab);
}

/*
 * Binary (DTB) representation
 */
int fdt_output(fdt_t fdt, uint8_t *dst)
{
    struct fdt_header *h;
    struct fdt_reserve_entry *re;
    int dt_struct_size;
    int dt_strings_size;
    int pos;

    /* Check tree is balanced */
    assert(fdt->open_node_count == 0);

    fdt_put_u32(fdt, FDT_END);

    dt_struct_size = fdt->dt_struct_len * sizeof(uint32_t);
    dt_strings_size = fdt->dt_strings_len;

    /* Output header */
    h = (struct fdt_header *)dst;
    h->magic = bswap_32(FDT_MAGIC);
    h->version = bswap_32(FDT_VERSION);
    h->last_comp_version = bswap_32(16);
    h->boot_cpuid_phys = bswap_32(0);
    h->size_dt_strings = bswap_32(dt_strings_size);
    h->size_dt_struct = bswap_32(dt_struct_size);
    pos = sizeof(struct fdt_header);
    h->off_dt_struct = bswap_32(pos);

    /* Output device-tree structure */
    memcpy(dst + pos, fdt->dt_struct, dt_struct_size);
    pos += dt_struct_size;

    /* Align to 8 byte boundary*/
    while ((pos & 7) != 0) {
        dst[pos++] = 0;
    }

    /* Memory reserve map */
    h->off_mem_rsvmap = bswap_32(pos);
    re = (struct fdt_reserve_entry *)(dst + pos);
    re->address = 0; /* no reserved entry */
    re->size = 0;
    pos += sizeof(struct fdt_reserve_entry);

    h->off_dt_strings = bswap_32(pos);

    /* Output device-tree strings */
    memcpy(dst + pos, fdt->dt_strings, dt_strings_size);
    pos += dt_strings_size;

    /* align to 8, just in case */
    while ((pos & 7) != 0) {
        dst[pos++] = 0;
    }

    h->totalsize = bswap_32(pos);
    return pos;
}

