/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Utilities
 */

#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <lib/utils.h>

void die(const char *err, ...)
{
    va_list params;

    va_start(params, err);
    vfprintf(stderr, err, params);
    fputs("\n", stderr);
    va_end(params);

    exit(1);
}

void *xmallocz(size_t size)
{
    void *ptr;

    ptr = malloc(size);
    if (!ptr)
        die("malloc failed (tried to allocate %zu bytes)", size);

    memset(ptr, 0, size);

    return ptr;
}

FILE *xfopen(const char *path, const char *mode)
{
    FILE *fp = fopen(path, mode);

    if (!fp)
        die("fopen failed (tried to open file '%s'): %s",
            path, strerror(errno));

    return fp;
}
