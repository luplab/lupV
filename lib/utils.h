/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 *
 * Utilities
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

/*
 * Compiler-related directives
 */
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

#define force_inline    inline __attribute__((always_inline))
#define no_inline       __attribute__((noinline))
#define no_return       __attribute__((noreturn))

#define __xconcat(a, b)     a ## b
#define concat(a, b)        __xconcat(a, b)
#define concat3(a, b, c)    concat(concat(a, b), c)
#define concat4(a, b, c, d) concat(concat3(a, b, c), d)

#define __xstr(a)   #a
#define str(a)      __xstr(a)

/*
 * Error management
 */
no_return void die(const char *err, ...);

/*
 * Standard function wrappers
 */
void *xmallocz(size_t size);
FILE *xfopen(const char *path, const char *mode);

/*
 * Bit operations
 */
static inline int ctz32(uint32_t a)
{
    int i;
    if (a == 0)
        return 32;
    for(i = 0; i < 32; i++) {
        if ((a >> i) & 1)
            return i;
    }
    return 32;
}

static inline bool is_power_of_2(uint64_t a)
{
    if (!a)
        return false;
    return !(a & (a - 1));
}

/*
 * Number comparison
 */
#define max(a, b)           \
    ({                      \
        typeof(a) _a = (a); \
        typeof(b) _b = (b); \
        _a >= _b ? _a : _b; \
    })

#define min(a, b)           \
    ({                      \
        typeof(a) _a = (a); \
        typeof(b) _b = (b); \
        _a <= _b ? _a : _b; \
    })

/*
 * Alignment
 */
#define ALIGN(x, a)         ALIGN_MASK(x, (typeof(x))(a) - 1)
#define ALIGN_MASK(x, mask) (((x) + (mask)) & ~(mask))

#endif /* UTILS_H */
