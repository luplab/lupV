/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020 Joël Porquet-Lupine
 *
 * Machine management
 */

#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <cpu/cpu.h>
#include <devices/blk.h>
#include <devices/pic.h>
#include <devices/rng.h>
#include <devices/rtc.h>
#include <devices/sys.h>
#include <devices/tmr.h>
#include <devices/tty.h>
#define DBG_ITEM DBG_MACHINE
#include <lib/debug.h>
#include <lib/fdt.h>
#include <lib/gdb.h>
#include <lib/utils.h>
#include <platform/iomem.h>
#include <platform/machine.h>

/*
 * Memory map
 */

/* List of devices */
enum {
    /* Memory */
    RAM, ROM,
    /* I/O devices */
    BLK, PIC, RNG, RTC, SYS, TMR, TTY,
};

/* For each device, define its base memory address and size of register map */
static struct {
    phys_addr_t base, size;
} memmap[] = {
    /* Memory */
    [RAM] = {0x80000000, 0x0},          /* Size is runtime parameter */
    [ROM] = {0xFFFE0000, 0x00010000},   /* 64 KiB */
    /* I/O devices */
    [BLK] = {0x20000000, 0x00001000},   /* 4 KiB */
    /* Hole at 0x20001000 for missing IPI */
    [PIC] = {0x20002000, 0x00001000},   /* 4 KiB */
    [RNG] = {0x20003000, 0x00001000},   /* 4 KiB */
    [RTC] = {0x20004000, 0x00001000},   /* 4 KiB */
    [SYS] = {0x20005000, 0x00001000},   /* 4 KiB */
    [TMR] = {0x20006000, 0x00001000},   /* 4 KiB */
    [TTY] = {0x20007000, 0x00001000},   /* 4 KiB */
};


/*
 * Interrupt map, between I/O devices and PIC
 */

enum {
    PIC_TTY_IRQ = 0,
    PIC_BLK_IRQ = 1,
};

/* RISCV machine */
static void memory_init(uint64_t ram_size)
{
    uint32_t max_ram_size;

    assert(ram_size != 0 && (ram_size & PAGE_MASK) == 0);

    max_ram_size = memmap[RAM + 1].base - memmap[RAM].base;
    if (ram_size >= max_ram_size)
        die("RAM is too big, maximum is: %lu MiB", max_ram_size >> 20);

    /* Update RAM size */
    memmap[RAM].size = ram_size;

    iomem_register_ram(memmap[RAM].base, memmap[RAM].size);
    iomem_register_ram(memmap[ROM].base, memmap[ROM].size);
}

static phys_addr_t load_image(phys_addr_t addr, const char *filename)
{
    FILE *f;
    iomem_t r;
    int size;
    uint8_t *ptr;

    f = xfopen(filename, "rb");

    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);

    dbg_info("Loading binary image '%s' in RAM at 0x%" PRIxpa, filename, addr);

    r = iomem_get_region(addr);
    if (size > addr - iomem_region_addr(r) + iomem_region_size(r))
        die("Binary image too big: %s", filename);

    ptr = iomem_ram_get_ptr(r, addr);
    if (fread(ptr, 1, size, f) != size)
        die("Error reading binary image: %s", filename);

    fclose(f);

    return addr + size;
}

static void generate_dtb(uint8_t *dst, phys_addr_t kernel_entry)
{
    fdt_t fdt;
    enum {
        CPU_PHANDLE = 1,
        CLK_PHANDLE,
        PIC_PHANDLE,
        SYS_PHANDLE,
    };
    int size;

    /* Build FDT */
    fdt = fdt_create();

    /* / { */
    fdt_begin_node(fdt, "");
    fdt_prop_str(fdt, "model", "LupV - RISC-V based system emulator for education");
    fdt_prop_str(fdt, "compatible", "luplab,lupv");
    fdt_prop_u32(fdt, "#address-cells", 2);
    fdt_prop_u32(fdt, "#size-cells", 2);

    /* /chosen */
    fdt_begin_node(fdt, "chosen");
    fdt_prop_str(fdt, "stdout-path", "/soc/tty");
    fdt_prop_u64n(fdt, "kernel", 2, kernel_entry, 0);
    fdt_end_node(fdt); /* } /chosen */

    /* /cpus { */
    fdt_begin_node(fdt, "cpus");
    fdt_prop_u32(fdt, "#address-cells", 1);
    fdt_prop_u32(fdt, "#size-cells", 0);
    fdt_prop_u32(fdt, "timebase-frequency", TIMEBASE_FREQ);

    /* /cpus/cpu@0 { */
    fdt_begin_node_num(fdt, "cpu", 0);
    fdt_prop_str(fdt, "compatible", "riscv");
    fdt_prop_str(fdt, "device_type", "cpu");
    fdt_prop_u32(fdt, "reg", 0);
#if CPU_XLEN == 32
    fdt_prop_str(fdt, "riscv,isa", "rv32suimafdc");
    fdt_prop_str(fdt, "mmu-type", "riscv,sv32");
#else
    fdt_prop_str(fdt, "riscv,isa", "rv64suimafdc");
    fdt_prop_str(fdt, "mmu-type", "riscv,sv48");
#endif
    fdt_prop_str(fdt, "status", "okay");

    /* /cpus/cpu@0/interrupt-controller { */
    fdt_begin_node(fdt, "interrupt-controller");
    fdt_prop_str(fdt, "compatible", "riscv,cpu-intc");
    fdt_prop_blk(fdt, "interrupt-controller");
    fdt_prop_u32(fdt, "#interrupt-cells", 1);
    fdt_prop_u32(fdt, "phandle", CPU_PHANDLE);
    fdt_end_node(fdt); /* } /cpus/cpu@0/interrupt-controller */

    fdt_end_node(fdt); /* } /cpus/cpu@0 */

    fdt_end_node(fdt); /* } /cpus */

    /* /memory@0x... { */
    fdt_begin_node_num(fdt, "memory", memmap[RAM].base);
    fdt_prop_str(fdt, "device_type", "memory");
    fdt_prop_u64n(fdt, "reg", 2, memmap[RAM].base, memmap[RAM].size);
    fdt_end_node(fdt); /* } /memory@0x... */

    /* /clk { */
    fdt_begin_node(fdt, "clk");
    fdt_prop_u32(fdt, "phandle", CLK_PHANDLE);
    fdt_prop_str(fdt, "compatible", "fixed-clock");
    fdt_prop_u32(fdt, "#clock-cells", 0);
    fdt_prop_u32(fdt, "clock-frequency", TIMEBASE_FREQ);
    fdt_end_node(fdt); /* } /clk */

    /* /soc { */
    fdt_begin_node(fdt, "soc");
    fdt_prop_str(fdt, "compatible", "simple-bus");
    fdt_prop_u32(fdt, "#address-cells", 2);
    fdt_prop_u32(fdt, "#size-cells", 2);
    fdt_prop_blk(fdt, "ranges");

    /* /soc/blk@0x... */
    fdt_begin_node_num(fdt, "blk", memmap[BLK].base);
    fdt_prop_str(fdt, "compatible", "lupio,blk");
    fdt_prop_u64n(fdt, "reg", 2, memmap[BLK].base, memmap[BLK].size);
    fdt_prop_u32n(fdt, "interrupts-extended", 2, PIC_PHANDLE, PIC_BLK_IRQ);
    fdt_end_node(fdt); /* } /soc/blk */

    /* /soc/pic@0x... { */
    fdt_begin_node_num(fdt, "pic", memmap[PIC].base);
    fdt_prop_str(fdt, "compatible", "lupio,pic");
    fdt_prop_blk(fdt, "interrupt-controller");
    fdt_prop_u32(fdt, "#interrupt-cells", 1);
    fdt_prop_u64n(fdt, "reg", 2, memmap[PIC].base, memmap[PIC].size);
    fdt_prop_u32n(fdt, "interrupts-extended", 2, CPU_PHANDLE, IRQ_S_EXT);
    fdt_prop_u32(fdt, "phandle", PIC_PHANDLE);
    fdt_end_node(fdt); /* } /soc/pic */

    /* /soc/rng@0x... */
    fdt_begin_node_num(fdt, "rng", memmap[RNG].base);
    fdt_prop_str(fdt, "compatible", "lupio,rng");
    fdt_prop_u64n(fdt, "reg", 2, memmap[RNG].base, memmap[RNG].size);
    fdt_end_node(fdt); /* } /soc/rng */

    /* /soc/rtc@0x... */
    fdt_begin_node_num(fdt, "rtc", memmap[RTC].base);
    fdt_prop_str(fdt, "compatible", "lupio,rtc");
    fdt_prop_u64n(fdt, "reg", 2, memmap[RTC].base, memmap[RTC].size);
    fdt_end_node(fdt); /* } /soc/rtc */

    /* /soc/sys@0x... */
    fdt_begin_node_num(fdt, "sys", memmap[SYS].base);
    fdt_prop_strn(fdt, "compatible", 2, "lupio,sys", "syscon");
    fdt_prop_u64n(fdt, "reg", 2, memmap[SYS].base, memmap[SYS].size);
    fdt_prop_u32(fdt, "phandle", SYS_PHANDLE);
    fdt_end_node(fdt); /* } /soc/sys */

    /* /soc/tmr@0x... { */
    fdt_begin_node_num(fdt, "tmr", memmap[TMR].base);
    fdt_prop_str(fdt, "compatible", "lupio,tmr");
    fdt_prop_u64n(fdt, "reg", 2, memmap[TMR].base, memmap[TMR].size);
    fdt_prop_u32n(fdt, "interrupts-extended", 2, CPU_PHANDLE, IRQ_S_TIMER);
    fdt_prop_u32(fdt, "clocks", CLK_PHANDLE);
    fdt_end_node(fdt); /* } /soc/tmr */

    /* /soc/tty@0x... { */
    fdt_begin_node_num(fdt, "tty", memmap[TTY].base);
    fdt_prop_str(fdt, "compatible", "lupio,tty");
    fdt_prop_u64n(fdt, "reg", 2, memmap[TTY].base, memmap[TTY].size);
    fdt_prop_u32n(fdt, "interrupts-extended", 2,
                  PIC_PHANDLE, PIC_TTY_IRQ);
    fdt_end_node(fdt); /* } /soc/tty */

    /* /soc/poweroff */
    fdt_begin_node(fdt, "poweroff");
    fdt_prop_str(fdt, "compatible", "syscon-poweroff");
    fdt_prop_u32(fdt, "regmap", SYS_PHANDLE);
    fdt_prop_u32(fdt, "offset", 0); /* LUPIO_SYS_HALT register */
    fdt_prop_u32(fdt, "value", 1);    /* exit(1) by default */
    fdt_end_node(fdt); /* } /soc/poweroff */

    /* /soc/reboot */
    fdt_begin_node(fdt, "reboot");
    fdt_prop_str(fdt, "compatible", "syscon-reboot");
    fdt_prop_u32(fdt, "regmap", SYS_PHANDLE);
    fdt_prop_u32(fdt, "offset", 4); /* LUPIO_SYS_REBT register */
    fdt_prop_u32(fdt, "value", 1);
    fdt_end_node(fdt); /* } /soc/poweroff */

    fdt_end_node(fdt); /* } /soc */

    fdt_end_node(fdt); /* } / */

    /* Generate DTB version in @dst */
    size = fdt_output(fdt, dst);

    if (dbg_item_get_level(DBG_ITEM) == DBG_LVL_MORE) {
        char fname[] = "lupv-XXXXXX.dtb";
        int fd = mkstemps(fname, 4);
        dbg_more("Output machine DTB to file '%s'", fname);
        write(fd, dst, size);
        close(fd);
    }

    fdt_destroy(fdt);
}

static void rom_init(phys_addr_t kernel_entry)
{
    uint32_t reset[8] = {0}, fdt_offset;
    iomem_t r;
    uint8_t *ptr;

    fdt_offset = sizeof(reset);

    /* Write reset vector in ROM */
    reset[0] = 0x297 + memmap[RAM].base - memmap[ROM].base; /* t0 = @RAM */
    reset[1] = 0x597;                                       /* a1 = pc */
    reset[2] = 0x58593 + ((fdt_offset - 4) << 20);          /* a1 = @fdt */
    reset[3] = 0xf1402573;                                  /* a0 = mhartid */
    reset[4] = 0x00028067;                                  /* jr t0 */

    r = iomem_get_region(memmap[ROM].base);
    ptr = iomem_ram_get_ptr(r, memmap[ROM].base);
    memcpy(ptr, reset, sizeof(reset));
    generate_dtb(ptr + fdt_offset, kernel_entry);
}

static void load_firmware(const char *bios, const char *kernel)
{
    phys_addr_t bios_end, kernel_entry;

    if (bios)
        /* Load bios at beginning of RAM */
        bios_end = load_image(memmap[RAM].base, bios);
    else
        bios_end = memmap[RAM].base;

    /* Followed by kernel (aligned on 4MiB boundary) */
    kernel_entry = ALIGN(bios_end, 4UL << 20);
    load_image(kernel_entry, kernel);

    /* Initalize ROM with small boot code and FDT */
    rom_init(kernel_entry);
}

void riscv_machine_init(struct machine_params *mp)
{
    stdio_init(mp->nsignals);
    if (mp->disk_filename)
        disk_img_init(mp->disk_filename, mp->disk_mode);

    memory_init(mp->ram_size);

    riscv_cpu_init(memmap[ROM].base);

    lupio_blk_init(memmap[BLK].base, memmap[BLK].size, PIC_BLK_IRQ);
    lupio_pic_init(memmap[PIC].base, memmap[PIC].size, IRQ_S_EXT);
    lupio_rng_init(memmap[RNG].base, memmap[RNG].size);
    lupio_rtc_init(memmap[RTC].base, memmap[RTC].size);
    lupio_sys_init(memmap[SYS].base, memmap[SYS].size);
    lupio_tmr_init(memmap[TMR].base, memmap[TMR].size, IRQ_S_TIMER);
    lupio_tty_init(memmap[TTY].base, memmap[TTY].size, PIC_TTY_IRQ);

    load_firmware(mp->bios_filename, mp->kernel_filename);
}

void riscv_machine_exit(void)
{
    stdio_exit();
    disk_img_exit();
    iomem_exit();
}

#define MAX_EXEC_CYCLE 500000
#define MAX_SLEEP_TIME 10       /* in milliseconds */

void riscv_machine_run(void)
{
    int64_t delay1;
    fd_set rfds;
    int fd_max, ret, delay;
    struct timeval tv;

    gdb_poll_interrupt();

    /* Determine how long to wait before having to emulate the system again */
    delay = 0;
    if (riscv_cpu_is_stalled())
        /* If CPU is stalled, then there's nothing to emulate, we can probably
         * wait until the next timer event */
        delay = MAX_SLEEP_TIME;
    /* Determine when is the next timer event */
    if (!riscv_cpu_has_timer_irq()) {
        delay1 = lupio_tmr_expiration();
        if (delay1 >= 0 && delay1 < delay)
            delay = delay1;
    }

    /* Prepare terminal to receive characters during the wait if possible */
    FD_ZERO(&rfds);
    fd_max = -1;
    if (lupio_tty_receive_ready()) {
        FD_SET(0, &rfds);
        fd_max = 0;
    }
    /* Convert delay in msecs to struct timeval in secs and usecs */
    tv.tv_sec = delay / 1000;
    tv.tv_usec = (delay % 1000) * 1000;

    /* Either wait until the next timer event, or wait until receiving a
     * character from the terminal */
    ret = select(fd_max + 1, &rfds, NULL, NULL, &tv);
    if (ret > 0 && FD_ISSET(0, &rfds))
        lupio_tty_receive_char();

    /* Emulate system for some cycles if CPU isn't stalled */
    if (!riscv_cpu_is_stalled())
        riscv_cpu_interp(MAX_EXEC_CYCLE);
}

