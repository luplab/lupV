/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2020 Joël Porquet-Lupine
 *
 * Machine management
 */

#ifndef MACHINE_H
#define MACHINE_H

#include <stdbool.h>
#include <stdint.h>

#include <devices/blk.h>

struct machine_params {
    uint64_t ram_size;
    bool nsignals;
    const char *bios_filename;
    const char *kernel_filename;
    enum disk_img_mode disk_mode;
    const char *disk_filename;
};

void riscv_machine_init(struct machine_params *mp);
void riscv_machine_exit(void);

void riscv_machine_run(void);

#endif /* MACHINE_H */
